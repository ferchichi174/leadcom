(function ($) {

	"use strict";

	// =====================================================
	//      PRELOADER
	// =====================================================
	$(window).on("load", function () {

		'use strict';

		$('[data-loader="circle-side"]').fadeOut(); // will first fade out the loading animation
		$('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
		var $hero = $('.hero-home .content');
		var $hero_v = $('#hero_video .content ');
		$hero.find('h3, p, form').addClass('fadeInUp animated');
		$hero.find('.btn-1').addClass('fadeIn animated');
		$hero_v.find('.h3, p, form').addClass('fadeInUp animated');
		$(window).scroll();
	})

	// =====================================================
	//      BACK TO TOP BUTTON
	// =====================================================
	function scrollToTop() {
		$('html, body').animate({ scrollTop: 0 }, 500, 'easeInOutExpo');
	}

	$(window).on('scroll', function () {
		if ($(this).scrollTop() > 100) {
			$('#toTop').fadeIn('slow');
		} else {
			$('#toTop').fadeOut('slow');
		}
	});

	$('#toTop').on('click', function () {
		scrollToTop();
		return false;
	});

	// =====================================================
	//      NAVBAR
	// =====================================================
	$(window).on('scroll load', function () {

		if ($(window).scrollTop() >= 1) {
			$('.main-header').addClass('active');
		} else {
			$('.main-header').removeClass('active');
		}

	});

	// =====================================================
	//      STICKY SIDEBAR SETUP
	// =====================================================
	$('#mainContent, #sidebar').theiaStickySidebar({
		additionalMarginTop: 90
	});

	// =====================================================
	//      MOBILE MENU
	// =====================================================
	var $menu = $("nav#menu").mmenu({
		"extensions": ["pagedim-black", "theme-dark"], // "theme-dark" can be changed to: "theme-white"
		counters: true,
		keyboardNavigation: {
			enable: true,
			enhance: true
		},
		navbar: {
			title: 'MENU'
		},
		navbars: [{ position: 'bottom', content: ['<a href="#">© 2021 Costy</a>'] }]
	},
		{
			// configuration
			clone: true,
		});
	var $icon = $("#hamburger");
	var API = $menu.data("mmenu");
	$icon.on("click", function () {
		API.open();
	});
	API.bind("open:finish", function () {
		setTimeout(function () {
			$icon.addClass("is-active");
		}, 100);
	});
	API.bind("close:finish", function () {
		setTimeout(function () {
			$icon.removeClass("is-active");
		}, 100);
	});

	// =====================================================
	//      FAQ NICE SCROLL
	// =====================================================
	var position;

	$('a.nice-scroll-faq').on('click', function (e) {
		e.preventDefault();
		position = $($(this).attr('href')).offset().top - 125;
		$('body, html').animate({
			scrollTop: position
		}, 500, 'easeInOutExpo');
	});

	$('ul#faqNav li a').on('click', function () {
		$('ul#faqNav li a.active').removeClass('active');
		$(this).addClass('active');
	});

	// =====================================================
	//      FAQ ACCORDION
	// =====================================================
	function toggleChevron(e) {
		$(e.target).prev('.card-header').find('i.indicator').toggleClass('icon-minus icon-plus');
	}
	$('.faq-accordion').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);

	// =====================================================
	//      GALLERY
	// =====================================================
	// Single Image
	$('#openImage1').magnificPopup({
		items: {
			src: 'img/gallery/1.jpg',
			title: 'Image related to Option Single 1'
		},
		type: 'image',
		fixedContentPos: false,
	});

	$('#openSimpleMailSummaryImage').magnificPopup({
		items: {
			src: 'img/presentation/simple-mail-summary.jpg',
			title: 'Simple Mail Summary'
		},
		type: 'image',
		fixedContentPos: false,
	});

	// Single Video
	$('#openVideo1').magnificPopup({
		items: {
			src: 'https://vimeo.com/432854555'
		},
		type: 'iframe',
		fixedContentPos: false,
	});

	// Image Gallery
	$('#openGallery1').magnificPopup({
		items: [
			{
				src: 'img/gallery/1.jpg',
				title: 'Image related to Option 1.1'
			},
			{
				src: 'img/gallery/2.jpg',
				title: 'Image related to Option 1.2'
			},
			{
				src: 'img/gallery/3.jpg',
				title: 'Image related to Option 1.3'
			}
		],
		gallery: {
			enabled: true
		},
		type: 'image',
		fixedContentPos: false,
	});

	// =====================================================
	//      CALCULATOR ELEMENTS
	// =====================================================

	// Function to format item prices usign priceFormat plugin
	function formatItemPrice() {
		$('.price').priceFormat({
			prefix: '€ ',
			centsSeparator: '.',
			thousandsSeparator: ','
		});
	}

	// Function to format total price usign priceFormat plugin
	function formatTotalPrice() {
		$('#total').priceFormat({
			prefix: '€ ',
			centsSeparator: '.',
			thousandsSeparator: ','
		});
	}

	// Variables for showing prices next to each dropdown item which has price
	var itemPrice = 0;

	// Function to show prices next to each dropdown item which has price
	function showItemPrices(optionGroupListName) {

		if (optionGroupListName == 'optionGroup1List') {
			$('#optionGroup1 .price-list .list li').each(function () {
				itemPrice = $(this).data('value');
				if (itemPrice != 0) { $(this).append('<span class="price">' + itemPrice + '</span>'); }
				formatItemPrice();
			});
		}

	}


	// Function to set total title and price initially
	function setTotalOnStart() {

		$('#totalTitle').val('Total');
		$('#total').val('€ 0.00');

	}

	// Variables for showing the price on the right of the selected dropdown item
	var selectedOptionItem = '';
	var selectedOption = '';
	var selectedItemPrice = 0;








    // Function to show the price on the right of the selected dropdown item
	function showSelectedItemPrice(optionGroupName) {
		selectedOptionItem = $(optionGroupName + ' option:selected');
		selectedOption = selectedOptionItem.text();
		selectedItemPrice = selectedOptionItem.val();

		if (selectedItemPrice != 0) {
			$(optionGroupName + ' .current').html(selectedOption + '<span class="price">' + selectedItemPrice + '</span>');
            document.getElementById("boxprice").innerHTML = "<sup>€</sup>"+selectedItemPrice;
            formatItemPrice();
		}
	}
    function showPrice(optionGroupName) {
        selectedOptionItem = $(optionGroupName + ' option:selected');
        selectedOption = selectedOptionItem.text();
        selectedItemPrice = selectedOptionItem.val();

        if (selectedItemPrice != 0) {
            alert(selectedItemPrice);
            document.getElementById("boxprice").innerHTML = "dfg"+selectedItemPrice;
            formatItemPrice();
        }
    }

	// Variables for calculation
	var chooseItemText = 'Select';

	var selectedItem1Title = '';
	var selectedItem1Price = 0;
	var actualQty1 = 0;
	var subSum1 = 0;

	var total = 0;
    var extraOption1IsChecked = false;
    var extraOption1Title = '';
    var extraOption1Price = 0;

    var extraOption2IsChecked = false;
    var extraOption2Title = '';
    var extraOption2Price = 0;

    var extraOption3IsChecked = false;
    var extraOption3Title = '';
    var extraOption3Price = 0;

    var extraOption4IsChecked = false;
    var extraOption4Title = '';
    var extraOption4Price = 0;

    var extraOption5IsChecked = false;
    var extraOption5Title = '';
    var extraOption5Price = 0;

    var extraOption6IsChecked = false;
    var extraOption6Title = '';
    var extraOption6Price = 0;

    var extraOption7IsChecked = false;
    var extraOption7Title = '';
    var extraOption7Price = 0;

	// Function to manage the calculations and update summary
	function updateSummary() {

        extraOption1IsChecked = $('#extraOption1').is(':checked');
        extraOption1Title = $('#extraOption1Title').text();
        extraOption1Price = $('#extraOption1').val();

        if (extraOption1IsChecked) {
            extraOption1Price = extraOption1Price * 1;
            $('#extraOption1Sum').html('<a href="javascript:;" id="extraOption1SumReset"><i class="fa fa-times-circle"></i></a> ' + extraOption1Title + '<span class="price">' + extraOption1Price.toFixed(2) + '</span>');
            formatItemPrice();

        } else { // If option in not checked

            extraOption1Price = 0;
            clearSummaryLine('extraOption1Sum');

        }

        extraOption2IsChecked = $('#extraOption2').is(':checked');
        extraOption2Title = $('#extraOption2Title').text();
        extraOption2Price = $('#extraOption2').val();

        if (extraOption2IsChecked) {
            extraOption2Price = extraOption2Price * 1;
            $('#extraOption2Sum').html('<a href="javascript:;" id="extraOption2SumReset"><i class="fa fa-times-circle"></i></a> ' + extraOption2Title + '<span class="price">' + extraOption2Price.toFixed(2) + '</span>');
            formatItemPrice();

        } else { // If option in not checked

            extraOption2Price = 0;
            clearSummaryLine('extraOption2Sum');

        }


        extraOption3IsChecked = $('#extraOption3').is(':checked');
        extraOption3Title = $('#extraOption3Title').text();
        extraOption3Price = $('#extraOption3').val();

        if (extraOption3IsChecked) {
            extraOption3Price = extraOption3Price * 1;
            $('#extraOption3Sum').html('<a href="javascript:;" id="extraOption3SumReset"><i class="fa fa-times-circle"></i></a> ' + extraOption3Title + '<span class="price">' + extraOption3Price.toFixed(2) + '</span>');
            formatItemPrice();

        } else { // If option in not checked

            extraOption3Price = 0;
            clearSummaryLine('extraOption3Sum');

        }

        extraOption4IsChecked = $('#extraOption4').is(':checked');
        extraOption4Title = $('#extraOption4Title').text();
        extraOption4Price = $('#extraOption4').val();

        if (extraOption4IsChecked) {
            extraOption4Price = extraOption4Price * 1;
            $('#extraOption4Sum').html('<a href="javascript:;" id="extraOption3SumReset"><i class="fa fa-times-circle"></i></a> ' + extraOption4Title + '<span class="price">' + extraOption4Price.toFixed(2) + '</span>');
            formatItemPrice();

        } else { // If option in not checked

            extraOption4Price = 0;
            clearSummaryLine('extraOption4Sum');

        }

        extraOption5IsChecked = $('#extraOption5').is(':checked');
        extraOption5Title = $('#extraOption5Title').text();
        extraOption5Price = $('#extraOption5').val();

        if (extraOption5IsChecked) {
            extraOption5Price = extraOption5Price * 1;
            $('#extraOption5Sum').html('<a href="javascript:;" id="extraOption5SumReset"><i class="fa fa-times-circle"></i></a> ' + extraOption5Title + '<span class="price">' + extraOption5Price.toFixed(2) + '</span>');
            formatItemPrice();

        } else { // If option in not checked

            extraOption5Price = 0;
            clearSummaryLine('extraOption5Sum');

        }

        extraOption6IsChecked = $('#extraOption6').is(':checked');
        extraOption6Title = $('#extraOption6Title').text();
        extraOption6Price = $('#extraOption6').val();

        if (extraOption6IsChecked) {
            extraOption6Price = extraOption6Price * 1;
            $('#extraOption6Sum').html('<a href="javascript:;" id="extraOption6SumReset"><i class="fa fa-times-circle"></i></a> ' + extraOption6Title + '<span class="price">' + extraOption6Price.toFixed(2) + '</span>');
            formatItemPrice();

        } else { // If option in not checked

            extraOption6Price = 0;
            clearSummaryLine('extraOption6Sum');

        }

        extraOption7IsChecked = $('#extraOption7').is(':checked');
        extraOption7Title = $('#extraOption7Title').text();
        extraOption7Price = $('#extraOption7').val();

        if (extraOption7IsChecked) {
            extraOption7Price = extraOption7Price * 1;
            $('#extraOption7Sum').html('<a href="javascript:;" id="extraOption7SumReset"><i class="fa fa-times-circle"></i></a> ' + extraOption7Title + '<span class="price">' + extraOption7Price.toFixed(2) + '</span>');
            formatItemPrice();

        } else { // If option in not checked

            extraOption7Price = 0;
            clearSummaryLine('extraOption7Sum');

        }

		// Get the current data from optionGroup1 elements
		selectedItem1Title = $('#optionGroup1List option:selected').text();
		selectedItem1Price = $('#optionGroup1List option:selected').val();
        if ($('#optionGroup1List option:selected').val() == "2.50"){



            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty').val();
            if (actualQty1 < 850){
                selectedItem1Price = 2.5;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>2.5";

            }
            else if ( 5000 < actualQty1  ) {
                selectedItem1Price = 0.5;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>0.5";
            }
            else if (850 < actualQty1 < 5000 ) {
                selectedItem1Price = 0.85;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>0.85";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1 + extraOption2Price + extraOption3Price +extraOption4Price + extraOption5Price+ extraOption6Price + extraOption7Price + extraOption1Price;

            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }
        if ($('#optionGroup1List option:selected').val() == "160.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty1').val();
            if (actualQty1 < 10){
                selectedItem1Price = 160;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>160";

            }
            else if ( actualQty1 > 50 ) {
                selectedItem1Price = 80;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>80";
            }
            else if (10 < actualQty1 < 50 ) {
                selectedItem1Price = 114;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>114";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1 + extraOption2Price + extraOption3Price +extraOption4Price + extraOption5Price+ extraOption6Price + extraOption7Price + extraOption1Price;

            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }

        if ($('#optionGroup1List option:selected').val() == "800.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty2').val();
            if (actualQty1 < 10){
                selectedItem1Price = 800;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>800";

            }
            else if ( actualQty1 > 50 ) {
                selectedItem1Price = 400;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>400";
            }
            else if (10 < actualQty1 < 50 ) {
                selectedItem1Price = 570;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>570";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1 + extraOption2Price + extraOption3Price +extraOption4Price + extraOption5Price+ extraOption6Price + extraOption7Price + extraOption1Price;

            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }

        if ($('#optionGroup1List option:selected').val() == "89.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty4').val();
            if (actualQty1 < 30){
                selectedItem1Price = 89;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>89";

            }
            else if ( actualQty1 > 200 ) {
                selectedItem1Price = 49;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>49";
            }
            else if (30 < actualQty1 < 200 ) {
                selectedItem1Price = 65;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>65";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1 + extraOption2Price + extraOption3Price +extraOption4Price + extraOption5Price+ extraOption6Price + extraOption7Price + extraOption1Price;

            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }
        if ($('#optionGroup1List option:selected').val() == "120.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty5').val();
            if (actualQty1 < 50){
                selectedItem1Price = 120;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>120";

            }
            else if ( actualQty1 > 150 ) {
                selectedItem1Price = 67;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>67";
            }
            else if (50 < actualQty1 < 150 ) {
                selectedItem1Price = 89;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>89";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }
        if ($('#optionGroup1List option:selected').val() == "90.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty6').val();
            if (actualQty1 < 10){
                selectedItem1Price = 90;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>90";

            }
            else if ( actualQty1 > 50 ) {
                selectedItem1Price = 50;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>50";
            }
            else if (10 < actualQty1 < 50 ) {
                selectedItem1Price = 70;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>70";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }

        if ($('#optionGroup1List option:selected').val() == "250.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty7').val();
            if (actualQty1 < 10){
                selectedItem1Price = 250;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>250";

            }
            else if ( actualQty1 > 50 ) {
                selectedItem1Price = 138;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>138";
            }
            else if (10 < actualQty1 < 50 ) {
                selectedItem1Price = 175;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>175";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }
        if ($('#optionGroup1List option:selected').val() == "338.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty8').val();
            if (actualQty1 < 50){
                selectedItem1Price = 338;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>338";

            }
            else if ( actualQty1 > 200 ) {
                selectedItem1Price = 187;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>187";
            }
            else if (50 < actualQty1 < 200 ) {
                selectedItem1Price = 237;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>237";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }
        if ($('#optionGroup1List option:selected').val() == "15.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty9').val();
            if (actualQty1 < 100){
                selectedItem1Price = 15;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>15";

            }
            else if ( actualQty1 > 200 ) {
                selectedItem1Price = 9;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>9";
            }
            else if (100 < actualQty1 < 200 ) {
                selectedItem1Price = 12;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>12";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }
        if ($('#optionGroup1List option:selected').val() == "21.00"){


            selectedItem1Price = $('#optionGroup1List option:selected').val();
            actualQty1 = $('#optionGroup1Qty10').val();
            if (actualQty1 < 100){
                selectedItem1Price = 21;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>21";

            }
            else if ( actualQty1 > 300 ) {
                selectedItem1Price = 12;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>12";
            }
            else if (100 < actualQty1 < 300 ) {
                selectedItem1Price = 16;
                document.getElementById("boxprice").innerHTML = "<sup>€</sup>16";
            }

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);


            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }


            // Update total in order summary
            total = subSum1 + extraOption2Price + extraOption3Price +extraOption4Price + extraOption5Price+ extraOption6Price + extraOption7Price + extraOption1Price;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();

        }


	}


	// Function to save actual values with updating the hidden fields
	function saveState() {

		// Update hidden fields with optionGroup1 details
		$('#option1Title').val(selectedItem1Title);
		$('#option1Price').val(selectedItem1Price);
		$('#subSum1').val(subSum1);

		// Update hidden field total
		$('#totalDue').val(total);

	}

	// Function to clear line in order summary
	function clearSummaryLine(summaryLineName) {

		if (summaryLineName == 'all') {
			$('#optionGroup1Sum').html('');
		}
		if (summaryLineName == 'optionGroup1Sum') {
			$('#optionGroup1Sum').html('');
		}
        if (summaryLineName == 'extraOption1Sum') {
            $('#extraOption1Sum').html('');
        }
        if (summaryLineName == 'extraOption2Sum') {
            $('#extraOption2Sum').html('');
        }
        if (summaryLineName == 'extraOption3Sum') {
            $('#extraOption3Sum').html('');
        }
        if (summaryLineName == 'extraOption4Sum') {
            $('#extraOption4Sum').html('');
        }
        if (summaryLineName == 'extraOption5Sum') {
            $('#extraOption5Sum').html('');
        }
        if (summaryLineName == 'extraOption6Sum') {
            $('#extraOption6Sum').html('');
        }
        if (summaryLineName == 'extraOption7Sum') {
            $('#extraOption7Sum').html('');
        }


	}

	// Function to reset the given dropdown list
	function resetDropdown(optionGroupListName) {

		if (optionGroupListName == 'all') {
			$('#optionGroup1List').val(0).niceSelect('update');
		}
		if (optionGroupListName == 'optionGroup1List') {
			$('#optionGroup1List').val(0).niceSelect('update');
		}

	}

	// Function to re-validate total price
	function reValidateTotal() {

		$('#total').parsley().validate();
	}

	// =====================================================
	//      EVENTS
	// =====================================================

	// When optionGroup1List is changed
	$('#optionGroup1List').on('change', function () {
        if ($('#optionGroup1List option:selected').text() == "Lead standart"){
            $("#extraOptionGroup1").show();
            $("#extraOptionGroup2").show();
            $("#extraOptionGroup3").show();
            $("#extraOptionGroup4").show();
            $("#extraOptionGroup5").show();
            $("#extraOptionGroup6").show();
            $("#extraOptionGroup7").show();

        }
        else{
            $("#extraOptionGroup1").hide();
            $("#extraOptionGroup2").hide();
            $("#extraOptionGroup3").hide();
            $("#extraOptionGroup4").hide();
            $("#extraOptionGroup5").hide();
            $("#extraOptionGroup6").hide();
            $("#extraOptionGroup7").hide();
            document.getElementById("extraOption1").checked = false;
            document.getElementById("extraOption2").checked = false;
            document.getElementById("extraOption3").checked = false;
            document.getElementById("extraOption4").checked = false;
            document.getElementById("extraOption5").checked = false;
            document.getElementById("extraOption6").checked = false;
            document.getElementById("extraOption7").checked = false;
        }

	    if ($('#optionGroup1List option:selected').val() == 2.50){
        $("#mrigel").show();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel4").hide();
            $("#mrigel5").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();

	    }
	    else if ($('#optionGroup1List option:selected').val() == 160.00) {
            $("#mrigel").hide();
            $("#mrigel2").hide();
            $("#mrigel1").show();
        }
	    else if ($('#optionGroup1List option:selected').val() == 800) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").show();
        }
        else  if ($('#optionGroup1List option:selected').val() == 89.00){
            $("#mrigel6").hide();
            $("#mrigel4").show();
            $("#mrigel5").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel").hide();


        }
        else if ($('#optionGroup1List option:selected').val() == 120) {
            $("#mrigel").hide();
            $("#mrigel2").hide();
            $("#mrigel1").hide();
            $("#mrigel6").hide();
            $("#mrigel4").hide();
            $("#mrigel5").show();
        }

        else if ($('#optionGroup1List option:selected').val() == 90) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").show();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
        }

        else if ($('#optionGroup1List option:selected').val() == 250) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel7").show();
        }

        else if ($('#optionGroup1List option:selected').val() == 338) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").show();
            $("#mrigel9").hide();
        }

        else if ($('#optionGroup1List option:selected').val() == 15) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").show();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
        }
        else if ($('#optionGroup1List option:selected').val() == 21) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
            $("#mrigel10").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 175) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel12").hide();
            $("#mrigel11").show();
        }

        else if ($('#optionGroup1List option:selected').val() == 138) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 15) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
            $("#mrigel15").hide();
            $("#mrigel14").hide();
            $("#mrigel13").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 12) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
            $("#mrigel13").hide();
            $("#mrigel15").hide();
            $("#mrigel14").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 9) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
            $("#mrigel13").hide();
            $("#mrigel14").hide();
            $("#mrigel15").show();
        }

        else {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
        }


        showSelectedItemPrice('#optionGroup1');
		updateSummary();
		saveState();
		reValidateTotal();

	});

    // When extraOption1 is checked
    $('#extraOption1').on('click', function () {

        updateSummary();
        saveState();
        reValidateTotal();
    });

    $('#extraOption2').on('click', function () {

        updateSummary();
        saveState();
        reValidateTotal();
    });
    $('#extraOption3').on('click', function () {

        updateSummary();
        saveState();
        reValidateTotal();
    });
    $('#extraOption4').on('click', function () {

        updateSummary();
        saveState();
        reValidateTotal();
    });
    $('#extraOption5').on('click', function () {

        updateSummary();
        saveState();
        reValidateTotal();
    });
    $('#extraOption6').on('click', function () {

        updateSummary();
        saveState();
        reValidateTotal();
    });
    $('#extraOption7').on('click', function () {

        updateSummary();
        saveState();
        reValidateTotal();
    });

	// Delete line 1 in summary list
	$('#optionGroup1Sum').delegate('#optionGroup1SumReset', 'click', function () {
		clearSummaryLine('optionGroup1Sum');
		resetDropdown('optionGroup1List');
		showItemPrices('optionGroup1List');
		updateSummary();
		saveState();
		reValidateTotal();
	});

    $('#extraOption1Sum').delegate('#extraOption1SumReset', 'click', function () {
        clearSummaryLine('extraOption1Sum');
        resetCheckbox('extraOption1');
        updateSummary();
        saveState();
        reValidateTotal();
    });

    $('#extraOption2Sum').delegate('#extraOption2SumReset', 'click', function () {
        clearSummaryLine('extraOption2Sum');
        resetCheckbox('extraOption2');
        updateSummary();
        saveState();
        reValidateTotal();
    });

    $('#extraOption3Sum').delegate('#extraOption3SumReset', 'click', function () {
        clearSummaryLine('extraOption3Sum');
        resetCheckbox('extraOption3');
        updateSummary();
        saveState();
        reValidateTotal();
    });

    $('#extraOption4Sum').delegate('#extraOption4SumReset', 'click', function () {
        clearSummaryLine('extraOption4Sum');
        resetCheckbox('extraOption4');
        updateSummary();
        saveState();
        reValidateTotal();
    });

    $('#extraOption5Sum').delegate('#extraOption5SumReset', 'click', function () {
        clearSummaryLine('extraOption5Sum');
        resetCheckbox('extraOption5');
        updateSummary();
        saveState();
        reValidateTotal();
    });

    $('#extraOption6Sum').delegate('#extraOption6SumReset', 'click', function () {
        clearSummaryLine('extraOption6Sum');
        resetCheckbox('extraOption6');
        updateSummary();
        saveState();
        reValidateTotal();
    });

    $('#extraOption7Sum').delegate('#extraOption7SumReset', 'click', function () {
        clearSummaryLine('extraOption7Sum');
        resetCheckbox('extraOption7');
        updateSummary();
        saveState();
        reValidateTotal();
    });

	// If reset is clicked, set the selected item to default
	$('#resetBtn').on('click', function () {
		clearSummaryLine('all');
		resetDropdown('all');
		updateSummary();
		showItemPrices('optionGroup1List');

		scrollToTop();
	});

	// =====================================================
	//      INIT TOTAL
	// =====================================================
	setTotalOnStart();

	// =====================================================
	//      INIT DROPDOWNS
	// =====================================================
	$('select').niceSelect();
	showItemPrices('optionGroup1List');

	// =====================================================
	//      RANGE SLIDER 1
	// =====================================================



	var $range = $('#optionGroup1RangeSlider'),
		$input = $('#optionGroup1Qty'),
		instance;
	    var min = 1;
        var   max = 10000;


	$range.ionRangeSlider({
		skin: 'flat',
		type: 'single',
		min: min,
		max: max,

		from: 1,
		hide_min_max: true,
		onStart: function (data) {
			$input.prop('value', data.from);
		},
		onChange: function (data) {
			$input.prop('value', data.from);
			updateSummary();
			saveState();
		},
		onFinish: function () {
			selectedItem1Title = $('#optionGroup1List option:selected').text();
			if ($('#optionGroup1Qty').val() == 10000) {
				$('#alertModal1').modal();
			}
		}
	});

	instance = $range.data("ionRangeSlider");

	$input.on('input', function () {
		var val = $(this).prop('value');

		// Validate
		if (val < min) {
			val = min;
			$input.val(min);
		} else if (val > max) {
			val = max;
			$input.val(max);
		}

		instance.update({
			from: val
		});

		updateSummary();
		saveState();

	});
    var $ran = $('#optionGroup2RangeSlider'),
        $inp = $('#optionGroup1Qty1'),
        instan;
    var mi = 1;
    var   ma = 300;


    $ran.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: mi,
        max: ma,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $inp.prop('value', data.from);
        },
        onChange: function (data) {
            $inp.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty1').val() == ma) {
                $('#alertModal2').modal();
            }
        }
    });

    instan = $range.data("ionRangeSlider");

    $inp.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $inp.val(min);
        } else if (val > max) {
            val = max;
            $inp.val(max);
        }

        instance.update({
            from: val
        });

        updateSummary();
        saveState();


    });


    var $ran2 = $('#optionGroup3RangeSlider'),
        $inp2 = $('#optionGroup1Qty2'),
        instan2;
    var maxi = 1;
    var mini = 150;


    $ran2.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: maxi,
        max: mini,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $inp2.prop('value', data.from);
        },
        onChange: function (data) {
            $inp2.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal1').modal();
            }
        }
    });

    instan2 = $range.data("ionRangeSlider");

    $inp.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $inp2.val(min);
        } else if (val > max) {
            val = max;
            $inp2.val(max);
        }

        instan2.update({
            from: val
        });

        updateSummary();
        saveState();

    });




    var $range4 = $('#optionGroup4RangeSlider'),
        $input4 = $('#optionGroup1Qty4'),
        instance4;
    var min4 = 1;
    var max4 = 500;

    $range4.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: min4,
        max: max4,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $input4.prop('value', data.from);
        },
        onChange: function (data) {
            $input4.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty4').val() == 500) {
                $('#alertModal2').modal();
            }
        }
    });

    instance4 = $range4.data("ionRangeSlider");

    $input4.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input4.val(min);
        } else if (val > max) {
            val = max;
            $input4.val(max);
        }

        instance4.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range5 = $('#optionGroup5RangeSlider'),
        $input5 = $('#optionGroup1Qty5'),
        instance5;

    $range5.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 1,
        max: 500,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $input5.prop('value', data.from);
        },
        onChange: function (data) {
            $input5.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal1').modal();
            }
        }
    });

    instance5 = $range5.data("ionRangeSlider");

    $input5.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input5.val(min);
        } else if (val > max) {
            val = max;
            $input5.val(max);
        }

        instance5.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range6 = $('#optionGroup6RangeSlider'),
        $input6 = $('#optionGroup1Qty6'),
        instance6;
    var min = 1;
    var max = 150;

    $range6.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: min,
        max: max,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $input6.prop('value', data.from);
        },
        onChange: function (data) {
            $input6.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty6').val() == 150) {
                $('#alertModal2').modal();
            }
        }
    });

    instance6 = $range6.data("ionRangeSlider");

    $input6.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input6.val(min);
        } else if (val > max) {
            val = max;
            $input6.val(max);
        }

        instance6.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range7 = $('#optionGroup7RangeSlider'),
        $input7 = $('#optionGroup1Qty7'),
        instance7;


    $range7.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 1,
        max: 150,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $input7.prop('value', data.from);
        },
        onChange: function (data) {
            $input7.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal2').modal();
            }
        }
    });

    instance7 = $range7.data("ionRangeSlider");

    $input7.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input7.val(min);
        } else if (val > max) {
            val = max;
            $input7.val(max);
        }

        instance7.update({
            from: val
        });

        updateSummary();
        saveState();

    });



    var $range8 = $('#optionGroup8RangeSlider'),
        $input8 = $('#optionGroup1Qty8'),
        instance8;
    var min = 1;
    var max = 500;

    $range8.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: min,
        max: max,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $input8.prop('value', data.from);
        },
        onChange: function (data) {
            $input8.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal2').modal();
            }
        }
    });

    instance8 = $range8.data("ionRangeSlider");

    $input8.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input8.val(min);
        } else if (val > max) {
            val = max;
            $input8.val(max);
        }

        instance8.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range9 = $('#optionGroup9RangeSlider'),
        $input9 = $('#optionGroup1Qty9'),
        instance9;


    $range9.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 1,
        max: 500,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $input9.prop('value', data.from);
        },
        onChange: function (data) {
            $input9.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if  ($('#optionGroup1Qty9').val() == 500) {
                $('#alertModal2').modal();
            }
        }
    });

    instance9 = $range9.data("ionRangeSlider");

    $input9.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input9.val(min);
        } else if (val > max) {
            val = max;
            $input9.val(max);
        }

        instance9.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range10 = $('#optionGroup10RangeSlider'),
        $input10 = $('#optionGroup1Qty10'),
        instance10;


    $range10.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 1,
        max: 800,

        from: 1,
        hide_min_max: true,
        onStart: function (data) {
            $input10.prop('value', data.from);
        },
        onChange: function (data) {
            $input10.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal1').modal();
            }
        }
    });

    instance10 = $range10.data("ionRangeSlider");

    $input10.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input10.val(min);
        } else if (val > max) {
            val = max;
            $input10.val(max);
        }

        instance10.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range11 = $('#optionGroup11RangeSlider'),
        $input11 = $('#optionGroup1Qty11'),
        instance11;


    $range11.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 200,
        max: 500,

        from: 200,
        hide_min_max: true,
        onStart: function (data) {
            $input11.prop('value', data.from);
        },
        onChange: function (data) {
            $input11.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty11').val() == 500) {
                $('#alertModal1').modal();
            }
        }
    });

    instance11 = $range11.data("ionRangeSlider");

    $input11.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input11.val(min);
        } else if (val > max) {
            val = max;
            $input11.val(max);
        }

        instance11.update({
            from: val
        });

        updateSummary();
        saveState();

    });


    var $range12 = $('#optionGroup12RangeSlider'),
        $input12 = $('#optionGroup1Qty12'),
        instance12;


    $range12.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 500,
        max: 2000,

        from: 500,
        hide_min_max: true,
        onStart: function (data) {
            $input12.prop('value', data.from);
        },
        onChange: function (data) {
            $input12.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal1').modal();
            }
        }
    });

    instance12 = $range12.data("ionRangeSlider");

    $input12.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input12.val(min);
        } else if (val > max) {
            val = max;
            $input12.val(max);
        }

        instance12.update({
            from: val
        });

        updateSummary();
        saveState();

    });


    var $range13 = $('#optionGroup13RangeSlider'),
        $input13 = $('#optionGroup1Qty13'),
        instance13;


    $range13.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 100,
        max: 200,

        from: 100,
        hide_min_max: true,
        onStart: function (data) {
            $input13.prop('value', data.from);
        },
        onChange: function (data) {
            $input13.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty13').val() == 200) {
                $('#alertModal1').modal();
            }
        }
    });

    instance13 = $range13.data("ionRangeSlider");

    $input13.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input13.val(min);
        } else if (val > max) {
            val = max;
            $input13.val(max);
        }

        instance13.update({
            from: val
        });

        updateSummary();
        saveState();

    });



    var $range14 = $('#optionGroup14RangeSlider'),
        $input14 = $('#optionGroup1Qty14'),
        instance14;


    $range14.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 200,
        max: 500,

        from: 200,
        hide_min_max: true,
        onStart: function (data) {
            $input14.prop('value', data.from);
        },
        onChange: function (data) {
            $input14.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty14').val() == 500) {
                $('#alertModal2').modal();
            }
        }
    });

    instance14 = $range14.data("ionRangeSlider");

    $input14.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input14.val(min);
        } else if (val > max) {
            val = max;
            $input14.val(max);
        }

        instance14.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range15 = $('#optionGroup15RangeSlider'),
        $input15 = $('#optionGroup1Qty15'),
        instance15;


    $range15.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 500,
        max: 2000,

        from: 500,
        hide_min_max: true,
        onStart: function (data) {
            $input15.prop('value', data.from);
        },
        onChange: function (data) {
            $input15.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal1').modal();
            }
        }
    });

    instance15 = $range15.data("ionRangeSlider");

    $input15.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input15.val(min);
        } else if (val > max) {
            val = max;
            $input15.val(max);
        }

        instance15.update({
            from: val
        });

        updateSummary();
        saveState();

    });
	// =====================================================
	//      FORM LABELS
	// =====================================================
	new FloatLabels('#personalDetails', {
		style: 1
	});

	// =====================================================
	//      FORM INPUT VALIDATION
	// =====================================================

	// Quantity inputs
	$('.qty-input').on('keypress', function (event) {
		if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
			event.preventDefault();
		}
	});

	$('#optionGroup1Qty').on('keypress', function () {
		selectedItem1Title = $('#optionGroup1List option:selected').text();
		if ($('#optionGroup1Qty').val() == 850) {
			$('#alertModal1').modal();
		}
	});

	$('#optionGroup2Qty').on('keypress', function () {
		selectedItem2Title = $('#optionGroup2List option:selected').text();
		if (selectedItem2Title == chooseItemText) {
			$('#alertModal2').modal();
		}
	});

	$('#optionGroup3Qty').on('keypress', function () {
		selectedItem3Title = $('#optionGroup3List option:selected').text();
		if (selectedItem3Title == chooseItemText) {
			$('#alertModal3').modal();
		}
	});


	// Empty order validation
	window.Parsley.addValidator('emptyOrder', {
		validateString: function (value) {
			return value !== '€ 0.00';
		},
		messages: {
			en: 'Order is empty.'
		}
	});

	// Whole form validation
	$('#orderForm').parsley();

	// Clear parsley empty elements
	if ('#orderForm'.length > 0) {
		$('#orderForm').parsley().on('field:success', function () {
			$('ul.parsley-errors-list').not(':has(li)').remove();
		});
	}

})(window.jQuery);
