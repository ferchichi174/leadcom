(function ($) {


    "use strict";

    // =====================================================
    //      PRELOADER
    // =====================================================
    $(window).on("load", function () {
        'use strict';
        $('[data-loader="circle-side"]').fadeOut(); // will first fade out the loading animation
        $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
        var $hero = $('.hero-home .content');
        var $hero_v = $('#hero_video .content ');
        $hero.find('h3, p, form').addClass('fadeInUp animated');
        $hero.find('.btn-1').addClass('fadeIn animated');
        $hero_v.find('.h3, p, form').addClass('fadeInUp animated');
        $(window).scroll();
    })

    // =====================================================
    //      BACK TO TOP BUTTON
    // =====================================================
    function scrollToTop() {
        $('html, body').animate({ scrollTop: 0 }, 500, 'easeInOutExpo');
    }

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 100) {
            $('#toTop').fadeIn('slow');
        } else {
            $('#toTop').fadeOut('slow');
        }
    });

    $('#toTop').on('click', function () {
        scrollToTop();
        return false;
    });

    // =====================================================
    //      NAVBAR
    // =====================================================
    $(window).on('scroll load', function () {

        if ($(window).scrollTop() >= 1) {
            $('.main-header').addClass('active');
        } else {
            $('.main-header').removeClass('active');
        }

    });

    // =====================================================
    //      STICKY SIDEBAR SETUP
    // =====================================================
    $('#mainContent, #sidebar').theiaStickySidebar({
        additionalMarginTop: 90
    });

    // =====================================================
    //      MOBILE MENU
    // =====================================================
    var $menu = $("nav#menu").mmenu({
            "extensions": ["pagedim-black", "theme-dark"], // "theme-dark" can be changed to: "theme-white"
            counters: true,
            keyboardNavigation: {
                enable: true,
                enhance: true
            },
            navbar: {
                title: 'MENU'
            },
            navbars: [{ position: 'bottom', content: ['<a href="#">© 2021 Costy</a>'] }]
        },
        {
            // configuration
            clone: true,
        });
    var $icon = $("#hamburger");
    var API = $menu.data("mmenu");
    $icon.on("click", function () {
        API.open();
    });
    API.bind("open:finish", function () {
        setTimeout(function () {
            $icon.addClass("is-active");
        }, 100);
    });
    API.bind("close:finish", function () {
        setTimeout(function () {
            $icon.removeClass("is-active");
        }, 100);
    });

    // =====================================================
    //      FAQ NICE SCROLL
    // =====================================================
    var position;

    $('a.nice-scroll-faq').on('click', function (e) {
        e.preventDefault();
        position = $($(this).attr('href')).offset().top - 125;
        $('body, html').animate({
            scrollTop: position
        }, 500, 'easeInOutExpo');
    });

    $('ul#faqNav li a').on('click', function () {
        $('ul#faqNav li a.active').removeClass('active');
        $(this).addClass('active');
    });

    // =====================================================
    //      FAQ ACCORDION
    // =====================================================
    function toggleChevron(e) {
        $(e.target).prev('.card-header').find('i.indicator').toggleClass('icon-minus icon-plus');
    }
    $('.faq-accordion').on('hidden.bs.collapse shown.bs.collapse', toggleChevron);

    // =====================================================
    //      GALLERY
    // =====================================================
    // Single Image
    $('#openImage1').magnificPopup({
        items: {
            src: 'img/gallery/1.jpg',
            title: 'Image related to Option Single 1'
        },
        type: 'image',
        fixedContentPos: false,
    });

    $('#openSimpleMailSummaryImage').magnificPopup({
        items: {
            src: 'img/presentation/simple-mail-summary.jpg',
            title: 'Simple Mail Summary'
        },
        type: 'image',
        fixedContentPos: false,
    });

    // Single Video
    $('#openVideo1').magnificPopup({
        items: {
            src: 'https://vimeo.com/432854555'
        },
        type: 'iframe',
        fixedContentPos: false,
    });

    // Image Gallery
    $('#openGallery1').magnificPopup({
        items: [
            {
                src: 'img/gallery/1.jpg',
                title: 'Image related to Option 1.1'
            },
            {
                src: 'img/gallery/2.jpg',
                title: 'Image related to Option 1.2'
            },
            {
                src: 'img/gallery/3.jpg',
                title: 'Image related to Option 1.3'
            }
        ],
        gallery: {
            enabled: true
        },
        type: 'image',
        fixedContentPos: false,
    });

    // =====================================================
    //      CALCULATOR ELEMENTS
    // =====================================================

    // Function to format item prices usign priceFormat plugin
    function formatItemPrice() {
        $('.price').priceFormat({
            prefix: '€ ',
            centsSeparator: '.',
            thousandsSeparator: ','
        });
    }

    // Function to format total price usign priceFormat plugin
    function formatTotalPrice() {
        $('#total').priceFormat({
            prefix: '€ ',
            centsSeparator: '.',
            thousandsSeparator: ','
        });
    }

    // Variables for showing prices next to each dropdown item which has price
    var itemPrice = 0;

    // Function to show prices next to each dropdown item which has price
    function showItemPrices(optionGroupListName) {

        if (optionGroupListName == 'optionGroup1List') {
            $('#optionGroup1 .price-list .list li').each(function () {
                itemPrice = $(this).data('value');
                if (itemPrice != 0) { $(this).append('<span class="price">' + itemPrice + '</span>'); }
                formatItemPrice();
            });
        }

    }


    // Function to set total title and price initially
    function setTotalOnStart() {

        $('#totalTitle').val('Total');
        $('#total').val('€ 0.00');

    }

    // Variables for showing the price on the right of the selected dropdown item
    var selectedOptionItem = '';
    var selectedOption = '';
    var selectedItemPrice = 0;

    // Function to show the price on the right of the selected dropdown item
    function showSelectedItemPrice(optionGroupName) {
        selectedOptionItem = $(optionGroupName + ' option:selected');
        selectedOption = selectedOptionItem.text();
        selectedItemPrice = selectedOptionItem.val();

        if (selectedItemPrice != 0) {
            $(optionGroupName + ' .current').html(selectedOption + '<span class="price">' + selectedItemPrice + '</span>');
            document.getElementById("boxprice").innerHTML = "<sup>€</sup>"+selectedItemPrice;
            formatItemPrice();
        }
    }
    function showPrice(optionGroupName) {
        selectedOptionItem = $(optionGroupName + ' option:selected');
        selectedOption = selectedOptionItem.text();
        selectedItemPrice = selectedOptionItem.val();

        if (selectedItemPrice != 0) {
            alert(selectedItemPrice);
            document.getElementById("boxprice").innerHTML = "dfg"+selectedItemPrice;
            formatItemPrice();
        }
    }

    // Variables for calculation
    var chooseItemText = 'Select';

    var selectedItem1Title = '';
    var selectedItem1Price = 0;
    var actualQty1 = 0;
    var subSum1 = 0;

    var total = 0;

    // Function to manage the calculations and update summary
    function updateSummary() {

        // Get the current data from optionGroup1 elements
        selectedItem1Title = $('#optionGroup1List option:selected').text();
        selectedItem1Price = $('#optionGroup1List option:selected').val();
        if ($('#optionGroup1List option:selected').val() == 160.00) {
            actualQty1 = $('#optionGroup1Qty').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else if ($('#optionGroup1List option:selected').val() == 114.00) {
            actualQty1 = $('#optionGroup1Qty1').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }

        else if ($('#optionGroup1List option:selected').val() == 80.00) {
            actualQty1 = $('#optionGroup1Qty2').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }

        // -------------------------------------------


        else  if ($('#optionGroup1List option:selected').val() == 89.00) {
            actualQty1 = $('#optionGroup1Qty4').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }

        else  if ($('#optionGroup1List option:selected').val() == 65.00) {

            actualQty1 = $('#optionGroup1Qty5').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }

        else  if ($('#optionGroup1List option:selected').val() == 49.00) {
            actualQty1 = $('#optionGroup1Qty6').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else  if ($('#optionGroup1List option:selected').val() == 90.00) {
            actualQty1 = $('#optionGroup1Qty7').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else  if ($('#optionGroup1List option:selected').val() == 70.00) {
            actualQty1 = $('#optionGroup1Qty8').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else  if ($('#optionGroup1List option:selected').val() == 50.00) {
            actualQty1 = $('#optionGroup1Qty9').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else  if ($('#optionGroup1List option:selected').val() == 250.00) {
            actualQty1 = $('#optionGroup1Qty10').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else  if ($('#optionGroup1List option:selected').val() == 175.00) {
            actualQty1 = $('#optionGroup1Qty11').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else  if ($('#optionGroup1List option:selected').val() == 138.00) {
            actualQty1 = $('#optionGroup1Qty12').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }

        else  if ($('#optionGroup1List option:selected').val() == 15.00) {
            actualQty1 = $('#optionGroup1Qty13').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else  if ($('#optionGroup1List option:selected').val() == 12.00) {
            actualQty1 = $('#optionGroup1Qty14').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }
        else  if ($('#optionGroup1List option:selected').val() == 9.00) {
            actualQty1 = $('#optionGroup1Qty15').val();

            subSum1 = (selectedItem1Price * 1) * (actualQty1 * 1);

            // Update order summary with optionGroup1 details
            if ((selectedItem1Title != chooseItemText) && (actualQty1 != 0)) {

                $('#optionGroup1Sum').html('<a href="javascript:;" id="optionGroup1SumReset"><i class="fa fa-times-circle"></i></a> ' + selectedItem1Title + ' x ' + actualQty1 + '<span class="price">' + subSum1.toFixed(2) + '</span>');
                formatItemPrice();

            } else { // If optionGroup slider is 0
                clearSummaryLine('optionGroup1Sum');
            }

            // Update total in order summary
            total = subSum1;
            $('#total').val(total.toFixed(2));
            formatTotalPrice();
        }

    }


    // Function to save actual values with updating the hidden fields
    function saveState() {

        // Update hidden fields with optionGroup1 details
        $('#option1Title').val(selectedItem1Title);
        $('#option1Price').val(selectedItem1Price);
        $('#subSum1').val(subSum1);

        // Update hidden field total
        $('#totalDue').val(total);

    }

    // Function to clear line in order summary
    function clearSummaryLine(summaryLineName) {

        if (summaryLineName == 'all') {
            $('#optionGroup1Sum').html('');
        }
        if (summaryLineName == 'optionGroup1Sum') {
            $('#optionGroup1Sum').html('');
        }

    }

    // Function to reset the given dropdown list
    function resetDropdown(optionGroupListName) {

        if (optionGroupListName == 'all') {
            $('#optionGroup1List').val(0).niceSelect('update');
        }
        if (optionGroupListName == 'optionGroup1List') {
            $('#optionGroup1List').val(0).niceSelect('update');
        }

    }

    // Function to re-validate total price
    function reValidateTotal() {

        $('#total').parsley().validate();
    }

    // =====================================================
    //      EVENTS
    // =====================================================

    // When optionGroup1List is changed
    $('#optionGroup1List').on('change', function () {
        if ($('#optionGroup1List option:selected').val() == 160.00){

            $("#mrigel").show();
            $("#mrigel1").hide();
            $("#mrigel2").hide();

        }
        else if ($('#optionGroup1List option:selected').val() == 114) {
            $("#mrigel").hide();
            $("#mrigel2").hide();
            $("#mrigel1").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 80) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").show();
        }
        if ($('#optionGroup1List option:selected').val() == 89.00){

            $("#mrigel4").show();
            $("#mrigel1").hide();
            $("#mrigel5").hide();
            $("#mrigel6").hide();
            $("#mrigel2").hide();
            $("#mrigel").hide();

        }
        else if ($('#optionGroup1List option:selected').val() == 65) {
            $("#mrigel").hide();
            $("#mrigel2").hide();
            $("#mrigel1").hide();
            $("#mrigel6").hide();
            $("#mrigel4").hide();
            $("#mrigel5").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 49) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").show();
        }

        else if ($('#optionGroup1List option:selected').val() == 90) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel7").show();
        }

        else if ($('#optionGroup1List option:selected').val() == 70) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").show();
            $("#mrigel9").hide();
        }

        else if ($('#optionGroup1List option:selected').val() == 50) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 250) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
            $("#mrigel10").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 175) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel12").hide();
            $("#mrigel11").show();
        }

        else if ($('#optionGroup1List option:selected').val() == 138) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 15) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
            $("#mrigel15").hide();
            $("#mrigel14").hide();
            $("#mrigel13").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 12) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
            $("#mrigel13").hide();
            $("#mrigel15").hide();
            $("#mrigel14").show();
        }
        else if ($('#optionGroup1List option:selected').val() == 9) {
            $("#mrigel").hide();
            $("#mrigel1").hide();
            $("#mrigel2").hide();
            $("#mrigel5").hide();
            $("#mrigel4").hide();
            $("#mrigel6").hide();
            $("#mrigel7").hide();
            $("#mrigel8").hide();
            $("#mrigel9").hide();
            $("#mrigel10").hide();
            $("#mrigel11").hide();
            $("#mrigel12").hide();
            $("#mrigel13").hide();
            $("#mrigel14").hide();
            $("#mrigel15").show();
        }




        showSelectedItemPrice('#optionGroup1');
        updateSummary();
        saveState();
        reValidateTotal();

    });

    // Delete line 1 in summary list
    $('#optionGroup1Sum').delegate('#optionGroup1SumReset', 'click', function () {
        clearSummaryLine('optionGroup1Sum');
        resetDropdown('optionGroup1List');
        showItemPrices('optionGroup1List');
        updateSummary();
        saveState();
        reValidateTotal();
    });

    // If reset is clicked, set the selected item to default
    $('#resetBtn').on('click', function () {
        clearSummaryLine('all');
        resetDropdown('all');
        updateSummary();
        showItemPrices('optionGroup1List');

        scrollToTop();
    });

    // =====================================================
    //      INIT TOTAL
    // =====================================================
    setTotalOnStart();

    // =====================================================
    //      INIT DROPDOWNS
    // =====================================================
    $('select').niceSelect();
    showItemPrices('optionGroup1List');

    // =====================================================
    //      RANGE SLIDER 1
    // =====================================================



    var $range = $('#optionGroup1RangeSlider'),
        $input = $('#optionGroup1Qty'),
    instance;
   var min = 10;
   var max = 50;

    $range.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: min,
        max: max,

        from: 10,
        hide_min_max: true,
        onStart: function (data) {
            $input.prop('value', data.from);
        },
        onChange: function (data) {
            $input.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty').val() == 50) {
                $('#alertModal1').modal();
            }
        }
    });

    instance = $range.data("ionRangeSlider");

    $input.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input.val(min);
        } else if (val > max) {
            val = max;
            $input.val(max);
        }

        instance.update({
            from: val
        });

        updateSummary();
        saveState();

    });


    var $range4 = $('#optionGroup4RangeSlider'),
        $input4 = $('#optionGroup1Qty4'),
        instance4;
    var min4 = 30;
    var max4 = 200;

    $range4.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: min4,
        max: max4,

        from: 30,
        hide_min_max: true,
        onStart: function (data) {
            $input4.prop('value', data.from);
        },
        onChange: function (data) {
            $input4.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty4').val() == 200) {
                $('#alertModal1').modal();
            }
        }
    });

    instance4 = $range4.data("ionRangeSlider");

    $input4.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input4.val(min);
        } else if (val > max) {
            val = max;
            $input4.val(max);
        }

        instance4.update({
            from: val
        });

        updateSummary();
        saveState();

    });


    var $ran = $('#optionGroup2RangeSlider'),
        $inp = $('#optionGroup1Qty1'),
        instan;
    var mi = 50;
    var   ma = 150;


    $ran.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: mi,
        max: ma,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $inp.prop('value', data.from);
        },
        onChange: function (data) {
            $inp.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty1').val() == ma) {
                $('#alertModal2').modal();
            }
        }
    });

    instan = $range.data("ionRangeSlider");

    $inp.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $inp.val(min);
        } else if (val > max) {
            val = max;
            $inp.val(max);
        }

        instance.update({
            from: val
        });

        updateSummary();
        saveState();


    });


    var $ran2 = $('#optionGroup3RangeSlider'),
        $inp2 = $('#optionGroup1Qty2'),
        instan2;
    var maxi = 150;
    var mini = 2000;


    $ran2.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: maxi,
        max: mini,

        from: 150,
        hide_min_max: true,
        onStart: function (data) {
            $inp2.prop('value', data.from);
        },
        onChange: function (data) {
            $inp2.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal2').modal();
            }
        }
    });

    instan2 = $range.data("ionRangeSlider");

    $inp.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $inp2.val(min);
        } else if (val > max) {
            val = max;
            $inp2.val(max);
        }

        instan2.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range5 = $('#optionGroup5RangeSlider'),
        $input5 = $('#optionGroup1Qty5'),
        instance5;

    $range5.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 200,
        max: 500,

        from: 10,
        hide_min_max: true,
        onStart: function (data) {
            $input5.prop('value', data.from);
        },
        onChange: function (data) {
            $input5.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty5').val() == 500) {
                $('#alertModal1').modal();
            }
        }
    });

    instance5 = $range5.data("ionRangeSlider");

    $input5.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input5.val(min);
        } else if (val > max) {
            val = max;
            $input5.val(max);
        }

        instance5.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range6 = $('#optionGroup6RangeSlider'),
        $input6 = $('#optionGroup1Qty6'),
        instance6;
    var min = 500;
    var max = 2000;

    $range6.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: min,
        max: max,

        from: 10,
        hide_min_max: true,
        onStart: function (data) {
            $input6.prop('value', data.from);
        },
        onChange: function (data) {
            $input6.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal2').modal();
            }
        }
    });

    instance6 = $range6.data("ionRangeSlider");

    $input6.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input6.val(min);
        } else if (val > max) {
            val = max;
            $input6.val(max);
        }

        instance6.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range7 = $('#optionGroup7RangeSlider'),
        $input7 = $('#optionGroup1Qty7'),
        instance7;


    $range7.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 10,
        max: 50,

        from: 10,
        hide_min_max: true,
        onStart: function (data) {
            $input7.prop('value', data.from);
        },
        onChange: function (data) {
            $input7.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty7').val() == 50) {
                $('#alertModal1').modal();
            }
        }
    });

    instance7 = $range7.data("ionRangeSlider");

    $input7.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input7.val(min);
        } else if (val > max) {
            val = max;
            $input7.val(max);
        }

        instance7.update({
            from: val
        });

        updateSummary();
        saveState();

    });



    var $range8 = $('#optionGroup8RangeSlider'),
        $input8 = $('#optionGroup1Qty8'),
        instance8;
    var min = 50;
    var max = 150;

    $range8.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: min,
        max: max,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $input8.prop('value', data.from);
        },
        onChange: function (data) {
            $input8.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty8').val() == 150) {
                $('#alertModal2').modal();
            }
        }
    });

    instance8 = $range8.data("ionRangeSlider");

    $input8.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input8.val(min);
        } else if (val > max) {
            val = max;
            $input8.val(max);
        }

        instance8.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range9 = $('#optionGroup9RangeSlider'),
        $input9 = $('#optionGroup1Qty9'),
        instance9;


    $range9.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 150,
        max: 2000,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $input9.prop('value', data.from);
        },
        onChange: function (data) {
            $input9.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal1').modal();
            }
        }
    });

    instance9 = $range9.data("ionRangeSlider");

    $input9.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input9.val(min);
        } else if (val > max) {
            val = max;
            $input9.val(max);
        }

        instance9.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range10 = $('#optionGroup10RangeSlider'),
        $input10 = $('#optionGroup1Qty10'),
        instance10;


    $range10.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 50,
        max: 200,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $input10.prop('value', data.from);
        },
        onChange: function (data) {
            $input10.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty10').val() == 200) {
                $('#alertModal1').modal();
            }
        }
    });

    instance10 = $range10.data("ionRangeSlider");

    $input10.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input10.val(min);
        } else if (val > max) {
            val = max;
            $input10.val(max);
        }

        instance10.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range11 = $('#optionGroup11RangeSlider'),
        $input11 = $('#optionGroup1Qty11'),
        instance11;


    $range11.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 200,
        max: 500,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $input11.prop('value', data.from);
        },
        onChange: function (data) {
            $input11.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty11').val() == 500) {
                $('#alertModal1').modal();
            }
        }
    });

    instance11 = $range11.data("ionRangeSlider");

    $input11.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input11.val(min);
        } else if (val > max) {
            val = max;
            $input11.val(max);
        }

        instance11.update({
            from: val
        });

        updateSummary();
        saveState();

    });


    var $range12 = $('#optionGroup12RangeSlider'),
        $input12 = $('#optionGroup1Qty12'),
        instance12;


    $range12.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 500,
        max: 2000,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $input12.prop('value', data.from);
        },
        onChange: function (data) {
            $input12.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal1').modal();
            }
        }
    });

    instance12 = $range12.data("ionRangeSlider");

    $input12.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input12.val(min);
        } else if (val > max) {
            val = max;
            $input12.val(max);
        }

        instance12.update({
            from: val
        });

        updateSummary();
        saveState();

    });


    var $range13 = $('#optionGroup13RangeSlider'),
        $input13 = $('#optionGroup1Qty13'),
        instance13;


    $range13.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 100,
        max: 200,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $input13.prop('value', data.from);
        },
        onChange: function (data) {
            $input13.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty13').val() == 200) {
                $('#alertModal1').modal();
            }
        }
    });

    instance13 = $range13.data("ionRangeSlider");

    $input13.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input13.val(min);
        } else if (val > max) {
            val = max;
            $input13.val(max);
        }

        instance13.update({
            from: val
        });

        updateSummary();
        saveState();

    });



    var $range14 = $('#optionGroup14RangeSlider'),
        $input14 = $('#optionGroup1Qty14'),
        instance14;


    $range14.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 200,
        max: 500,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $input14.prop('value', data.from);
        },
        onChange: function (data) {
            $input14.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if ($('#optionGroup1Qty14').val() == 500) {
                $('#alertModal2').modal();
            }
        }
    });

    instance14 = $range14.data("ionRangeSlider");

    $input14.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input14.val(min);
        } else if (val > max) {
            val = max;
            $input14.val(max);
        }

        instance14.update({
            from: val
        });

        updateSummary();
        saveState();

    });

    var $range15 = $('#optionGroup15RangeSlider'),
        $input15 = $('#optionGroup1Qty15'),
        instance15;


    $range15.ionRangeSlider({
        skin: 'flat',
        type: 'single',
        min: 500,
        max: 2000,

        from: 50,
        hide_min_max: true,
        onStart: function (data) {
            $input15.prop('value', data.from);
        },
        onChange: function (data) {
            $input15.prop('value', data.from);

            updateSummary();
            saveState();
        },
        onFinish: function () {
            selectedItem1Title = $('#optionGroup1List option:selected').text();
            if (selectedItem1Title == chooseItemText) {
                $('#alertModal1').modal();
            }
        }
    });

    instance15 = $range15.data("ionRangeSlider");

    $input15.on('input', function () {
        var val = $(this).prop('value');

        // Validate
        if (val < min) {
            val = min;
            $input15.val(min);
        } else if (val > max) {
            val = max;
            $input15.val(max);
        }

        instance15.update({
            from: val
        });

        updateSummary();
        saveState();

    });



    // =====================================================
    //      FORM LABELS
    // =====================================================
    new FloatLabels('#personalDetails', {
        style: 1
    });

    // =====================================================
    //      FORM INPUT VALIDATION
    // =====================================================

    // Quantity inputs
    $('.qty-input').on('keypress', function (event) {
        if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
            event.preventDefault();
        }
    });

    $('#optionGroup1Qty').on('keypress', function () {
        selectedItem1Title = $('#optionGroup1List option:selected').text();
        if (selectedItem1Title == chooseItemText) {
            $('#alertModal1').modal();
        }
    });

    $('#optionGroup2Qty').on('keypress', function () {
        selectedItem2Title = $('#optionGroup2List option:selected').text();
        if (selectedItem2Title == chooseItemText) {
            $('#alertModal2').modal();
        }
    });

    $('#optionGroup3Qty').on('keypress', function () {
        selectedItem3Title = $('#optionGroup3List option:selected').text();
        if (selectedItem3Title == chooseItemText) {
            $('#alertModal3').modal();
        }
    });


    // Empty order validation
    window.Parsley.addValidator('emptyOrder', {
        validateString: function (value) {
            return value !== '€ 0.00';
        },
        messages: {
            en: 'Order is empty.'
        }
    });

    // Whole form validation
    $('#orderForm').parsley();

    // Clear parsley empty elements
    if ('#orderForm'.length > 0) {
        $('#orderForm').parsley().on('field:success', function () {
            $('ul.parsley-errors-list').not(':has(li)').remove();
        });
    }

})(window.jQuery);
