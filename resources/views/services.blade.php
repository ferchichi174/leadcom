@extends('layout')
@section('content')

    <section class="service-section padding-tb">
        <div class="container">

            <div class="section-wrapper">
                <div class="lab-item">
                    <a href=" {{ route('cost',['id'=>2,'gid'=>$gid]) }}">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/crm.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Lead d’entreprise pour CRM-ERP</h4>
                                <p>Hunky dory barney fanny around up the duff no bigge loo cup of tea joly good ruddy say arse.!</p>
                            </div>
                        </div></a>
                </div>
                <div class="lab-item">
                    <a href=" {{ route('cost',['id'=>3,'gid'=>$gid]) }}">
                        <div class="lab-inner">

                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/application.png') }}" alt="lab-thumb">
                            </div>

                            <div class="lab-content">
                                <h4>Lead d’entreprise pour Application</h4>
                                <p>Hunky dory barney fanny around up the duff no bigge loo cup of tea joly good ruddy say arse.!</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="lab-item">
                    <a href=" {{ route('cost',['id'=>1,'gid'=>$gid]) }}">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/modern-house.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Lead pour client de site vitrine</h4>
                                <p>Hunky dory barney fanny around up the duff no bigge loo cup of tea joly good ruddy say arse.!</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="lab-item">
                    <a href=" {{ route('cost',['id'=>4,'gid'=>$gid]) }}">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/online-shop.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Client de site E-commerce</h4>
                                <p>Hunky dory barney fanny around up the duff no bigge loo cup of tea joly good ruddy say arse.!</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="lab-item">
                    <a href=" {{ route('cost',['id'=>5,'gid'=>$gid]) }}">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/bullhorn.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Client Community manager et marketing digital</h4>
                                <p>Hunky dory barney fanny around up the duff no bigge loo cup of tea joly good ruddy say arse.!</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="lab-item">
                    <a href=" {{ route('cost',['id'=>6,'gid'=>$gid]) }}">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/curve.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4> Clients Création Graphique</h4>
                                <p>Hunky dory barney fanny around up the duff no bigge loo cup of tea joly good ruddy say arse.!</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

@endsection
