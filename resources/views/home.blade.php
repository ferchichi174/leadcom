@extends('layout')
@section('content')


    <!-- Banner Section start here -->
    <section class="banner-section style-6">
        <div class="banner-area">
            <div class="container">
                <div class="row no-gutters align-items-center">
                    <div class="col-lg-6 col-12">
                        <div class="content-part">
                            <div class="section-header style-2">
                                <h2>Le premier
                                    fournisseur de lead qualifier dédier aux agences.</h2>
                                <p>Nous vous fournissons du lead qualifé prêt a la vente pour tous type de service digitale.</p>
                                <a href="#" class="lab-btn"><span>Commander du Lead.</span></a>
                                <a href="{{ route('Quest') }}" class="lab-btn"><span>performance de mes leads</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="section-wrapper">
                            <div class="banner-thumb">
                                <img style ="width: 800px;" src="{{ asset('assets/images/banner/marketing/princ.png') }}" alt="marketing-banner">
                                <div class="thumb-shape">
                                    {{--                                <div class="th-shape th-shape-1"><img src="assets/images/banner/marketing/shape/02.png" alt="th-shape"></div>--}}
                                    <div class="th-shape th-shape-2"><img style="height: 165px;" src="{{ asset('assets/images/banner/marketing/shape/Client2.png') }}" alt="th-shape"></div>
                                    <div class="th-shape th-shape-3"><img style="height: 165px;" src="{{ asset('assets/images/banner/marketing/shape/Client3.png') }}" alt="th-shape"></div>
                                    <div class="th-shape th-shape-4"><img style="height: 165px;" src="{{ asset('assets/images/banner/marketing/shape/Client1.png') }} " alt="th-shape"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Section ending here -->


    <!-- About Section Start Here -->
    <section class="about-section style-2 style-3 style-4 padding-tb">
        <div class="container">
            <div class="section-header">
                <span>L’ACHAT DE LEAD QUALIFIÉ</span>
                <h2>Personnalisés et uniques.</h2>
            </div>
            <div class="section-wrapper">
                <div class="lab-item">
                    <div class="lab-inner">
                        <div class="lab-thumb">
                            <img style ="width: 37px;"src= "{{ asset('assets/images/marketing/prospection.png') }}" alt="lab-thumb">
                        </div>
                        <div class="lab-content">
                            <h4>Cyblage de vos prospec</h4>
                            <p>Identification de vos prospects / Nos équipes identifie les besoins en développement informatique ou Marketing digitale de chaque Entreprises partout dans le monde.</p>
                        </div>
                    </div>
                </div>
                <div class="lab-item">
                    <div class="lab-inner">
                        <div class="lab-thumb">
                            <img  style ="width: 37px;" src="{{ asset('assets/images/marketing/devis-4.png') }}" alt="lab-thumb">
                        </div>
                        <div class="lab-content">
                            <h4>Qualification de vos contact</h4>
                            <p>Qualification de vos contacts / Nous élaborons les premiers échanges avec vos futurs clients pour vous fournir un taux de conversion hautement qualitatif.</p>
                        </div>
                    </div>
                </div>
                <div class="lab-item">
                    <div class="lab-inner">
                        <div class="lab-thumb">
                            <img style ="width: 37px;" src="{{ asset('assets/images/marketing/Qualification.png') }}" alt="lab-thumb">
                        </div>
                        <div class="lab-content">
                            <h4>Signé vos devis et contrat clients.</h4>
                            <p>Vendez plus facilement. Vous n’avez plus qu’à signé votre client pour lui fournir le service digital attendu toutes les informations vous serons fournie.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Section Ending Here -->


    <!-- Marketing Range Section Start here -->
    <section class="market-range-section padding-tb">
        <div class="container">
            <div class="top-area">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-12">
                        <div class="mr-thumb">
                            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img src="{{ asset('assets/images/marketing/1.png') }}" class="d-block w-100" alt="...">
                                    </div>
                                    <div id="img2" class="carousel-item ">
                                        <img style="height: 600px;" src="{{ asset('assets/images/marketing/2.png') }}" class="d-block w-100" alt="...">
                                    </div>
                                    <div class="carousel-item">
                                        <img src="{{ asset('assets/images/marketing/1.png') }}" class="d-block w-100" alt="...">
                                    </div>
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="section-header style-2">
                            <span>DES LEAD QUALITATIFS ET DIVERSIFIER</span>
                            <h2>Boostez vos ventes grâce à nos taux de conversions.</h2>
                            <p>  Acheter des leads est devenu un enjeu majeur pour la croissance de votre agence de
                                communication ou de développement Web.
                                C'est pourquoi Leadcom.io vous assures un retour investissement garantie.
                                Nous vous mettons à dispositions nos connaissances et notre savoir-faire commercial pour
                                vous ramener le prospect qualifié. Dans la majorité des cas la vente ce fait à 90% grâce à le
                                qualité de nos équipes spécialisés dans le conseil et l'accompagnement Numérique.</p>
                        </div>
                        <div class="section-wrapper">
                            <div class="skill-bar-wrapper">
                                <div class="skill-item">
                                    <div class="skill-title">
                                        <div class="left">Contrat CRM / ERP.</div>
                                        <div class="right">85%</div>
                                    </div>
                                    <div class="skillbar-container clearfix" data-percent="85%">
                                        <div class="skills" style="background: linear-gradient(to top, #23cc88, #8ecf35);"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <div class="skill-title">
                                        <div id="news" class="left">Contrat Site E-commerce/vitrine</div>
                                        {{--                                    <a  data-target="carouselExampleControls" data-slide-to="img2" href="#">About<small>Lorem ipsum dolor sit</small></a>--}}
                                        <div class="right">70%</div>
                                    </div>
                                    <div class="skillbar-container clearfix" data-percent="70%">
                                        <div class="skills" style="background: linear-gradient(to top, #ff4f58, #ffb400);"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <div class="skill-title">
                                        <div class="left">Contrat Application Mobile.</div>
                                        <div class="right">60%</div>
                                    </div>
                                    <div class="skillbar-container clearfix" data-percent="60%">
                                        <div class="skills" style="background: linear-gradient(to top, #01cbad, #47a1f2);"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <div class="skill-title">
                                        <div class="left">Contrat Application Mobile.</div>
                                        <div class="right">60%</div>
                                    </div>
                                    <div class="skillbar-container clearfix" data-percent="60%">
                                        <div class="skills" style="background: linear-gradient(to top, #01cbad, #47a1f2);"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <div class="skill-title">
                                        <div class="left">Contrat Application Mobile.</div>
                                        <div class="right">60%</div>
                                    </div>
                                    <div class="skillbar-container clearfix" data-percent="60%">
                                        <div class="skills" style="background: linear-gradient(to top, #01cbad, #47a1f2);"></div>
                                    </div>
                                </div>
                                <div class="skill-item">
                                    <div class="skill-title">
                                        <div class="left">Contrat Application Mobile.</div>
                                        <div class="right">60%</div>
                                    </div>
                                    <div class="skillbar-container clearfix" data-percent="60%">
                                        <div class="skills" style="background: linear-gradient(to top, #01cbad, #47a1f2);"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-area">
                <div class="section-wrapper">
                    <div class="contact-count-item">
                        <div class="contact-count-inner">
                            <div class="contact-count-content">
                                <h5 style="color: #fb4f6b;"><span class="counter">370</span>+</h5>
                                <p>Nombre d’opération.</p>
                            </div>
                        </div>
                    </div>
                    <div class="contact-count-item">
                        <div class="contact-count-inner">
                            <div class="contact-count-content">
                                <h5 style="color: #ffb400;"><span class="counter">348</span></h5>
                                <p>Nombre de lead vendu.</p>
                            </div>
                        </div>
                    </div>
                    <div class="contact-count-item">
                        <div class="contact-count-inner">
                            <div class="contact-count-content">
                                <h5 style="color: #c961fa;"><span class="counter">25</span>+</h5>
                                <p>Nombre de lead signé </p>
                            </div>
                        </div>
                    </div>
                    <div class="contact-count-item">
                        <div class="contact-count-inner">
                            <div class="contact-count-content">
                                <h5 style="color: #38cd78;"><span class="counter">24</span>K +</h5>
                                <p>Nombre de client satisfait</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Marketing Range Section Start here -->

    <div class="service-pricing padding-tb " style="margin-top:181px">
        <!-- Service Section Start Here -->
        <section class="service-section style-2 style-6">
            <div class="container">
                <div class="section-header">
                    <span>what we do</span>
                    <h2>All Kind of Marketing Solutions For You</h2>
                </div>
                <div class="section-wrapper">
                    <div class="lab-item">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img  style="width: 57px;" src="{{ asset('assets/images/service/marketing/crm.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Lead d’entreprise pour CRM-ERP</h4>
                                <p>Commandez du lead pour des projets valorisant pour vous et vos équipes.</p>
                            </div>
                        </div>
                    </div>
                    <div class="lab-item">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img  style="width: 57px;" src="{{ asset('assets/images/service/marketing/application.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Lead d’entreprise pour Application</h4>
                                <p>Commandez du lead aux prêt de jeunes start-up souhaitant développer leur projet
                                    d'application proposer leurs vos compétences et votre expérience pour la réalisation de leur
                                    projet.</p>
                            </div>
                        </div>
                    </div>
                    <div class="lab-item">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/modern-house.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Lead pour client de site vitrine</h4>
                                <p>Commandez votre lead et signé vos devis auprès d’artisan commerçant ou
                                    profession libéral et bien d’autre secteur d’activité attendant vos conseils
                                    et votre accompagnement dans le monde du numérique.</p>
                            </div>
                        </div>
                    </div>
                    <div class="lab-item">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/online-shop.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Client de site E-commerce</h4>
                                <p>Commandez votre lead de commerçant souhaitant digitaliser leurs
                                    commerces, achetez et récupéré leurs contacts afin de leurs proposé votre
                                    service.</p>
                            </div>
                        </div>
                    </div>
                    <div class="lab-item">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/bullhorn.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Client Community manager et marketing digital</h4>
                                <p>Commandez votre lead de client cherchant des services de marketing digitale et community
                                    manager proposé leurs des services pour entretenir et augmenter leurs communautés en ligne.</p>
                            </div>
                        </div>
                    </div>
                    <div class="lab-item">
                        <div class="lab-inner">
                            <div class="lab-thumb">
                                <img style="width: 57px;" src="{{ asset('assets/images/service/marketing/curve.png') }}" alt="lab-thumb">
                            </div>
                            <div class="lab-content">
                                <h4>Clients Création Graphique</h4>
                                <p>Commandez du lead d'entreprise avec des besoins continuels de support graphiques vidéo et plaquette commerciale numérique font intégralement partie des budgets dédier à la communication des entreprises saisies l'occasion de leurs proposé vos services.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Service Section Ending Here -->
        <!-- Pricing Table Section Start Here -->
        <section class="pricing-table marketing padding-tb pb-0">
            <div class="container">
                <div class="section-header">
                    <span>Pricing tabile</span>
                    <h2>Choose Your Best Pricing Plan</h2>
                </div>
                <div class="section-wrapper">
                    <div class="pricing-item">
                        <div class="pricing-inner">
                            <div class="pricing-head">
                                <h4>Lead cyblée</h4>
                                <p>Obtenez de nouveau contacte pour enrichir votre répertoire afin de
                                    proposer vos services Numérique.
                                    Vous avez-la possibilité d’enrichir les informations de votre lead.</p>
                            </div>
                            <div class="pricing-body">
                                <div class="price">
                                    <h2>€0.50 </h2>
                                    <p>Per-Lead</p>
                                </div>
                                <div class="price-list">
                                    <ul>
                                        <li>Choix du secteur d’activité 5 secteurs</li>
                                        <li>Adresse postale</li>
                                        <li>Mail</li>
                                        <li>Nom de l’entreprise</li>
                                        <li>Réseaux sociaux</li>
                                        <li>Site internet</li>
                                    </ul>
                                </div>
                                <div class="price-btn">
                                    <a href="{{route('services',['id'=>1])}}" class="lab-btn"><span>Commander</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pricing-item">
                        <div class="pricing-inner">
                            <div class="pricing-head">
                                <h4>Lead qualifié</h4>
                                <p>L’offre la plus populaire avec un taux de conversion de nos lead de plus de 80% information complète pour vous assurer un rendement et succès garantie.</p>
                            </div>
                            <div class="pricing-body">
                                <div class="price">
                                    <h2>€160 </h2>
                                    <p>Per-Lead</p>
                                </div>
                                <div class="price-list">
                                    <ul>
                                        <li>Choix du secteur d’activité illimité</li>
                                        <li>Adresse postale</li>
                                        <li>Mail</li>
                                        <li>Nom de l’entreprise</li>
                                        <li>Type de service Numérique</li>
                                        <li>Budget de votre prospect</li>
                                        <li>Chiffre d’affaire de votre prospect</li>
                                        <li>Nom/prénom du gérant</li>
                                        <li>Réseaux sociaux</li>
                                        <li>Site internet de l’entreprise</li>
                                    </ul>
                                </div>
                                <div class="price-btn">
                                    <a href="{{route('services',['id'=>2])}}" class="lab-btn"><span>Commander</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pricing-item">
                        <div class="pricing-inner">
                            <div class="pricing-head">
                                <h4>Lead qualifié</h4>
                                <p>L’offre la plus qualitative elle vous permet d’optimiser vos équipe
                                    commerciale, nous vous offrons l’opportunité d’investir sur vos future
                                    clients  les rendez-vous sont garantie avec un tôt de conversion de 90%.</p>
                            </div>
                            <div class="pricing-body">
                                <div class="price">
                                    <h2>€800 </h2>
                                    <p>Per-Lead</p>
                                </div>
                                <div class="price-list">
                                    <ul>
                                        <li>Choix du secteur d’activité illimité</li>
                                        <li>Adresse postale</li>
                                        <li>Mail</li>
                                        <li>Nom de l’entreprise</li>
                                        <li>Type de service Numérique</li>
                                        <li>Budget de votre prospect</li>
                                        <li>Chiffre d’affaire de votre prospect</li>
                                        <li>Nom/prénom du gérant</li>
                                        <li>Réseaux sociaux</li>
                                        <li>Site internet de l’entreprise</li>
                                        <li>Prise de rendez-vous sur votre iclandly</li>
                                    </ul>
                                </div>
                                <div class="price-btn">
                                    <a href="{{route('services',['id'=>3])}}" class="lab-btn"><span>Commander</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Pricing Table Section Ending Here -->
    </div>

    <!-- testimonial section start here -->
    <section class="testimonial-section style-2 padding-tb">
        <div class="container">
            <div class="section-header">
                <span>OUR TESTIMONIALS</span>
                <h2>Our Customer Always Happy With Our Services</h2>
            </div>
            <div class="section-wrapper">
                <div class="testimonial-slider-two">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active">
                                <img src="assets/images/team/01.jpg" alt="testimonial">
                            </li>
                            <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1">
                                <img src="assets/images/team/02.jpg" alt="testimonial">
                            </li>
                            <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2">
                                <img src="assets/images/team/03.jpg" alt="testimonial">
                            </li>
                            <li data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3">
                                <img src="assets/images/team/04.jpg" alt="testimonial">
                            </li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="testi-item">
                                    <div class="testi-inner">
                                        <div class="testi-thumb" >
                                            <img src="{{ asset('assets/images/testimonial/quote/marketing/1.png') }}" alt="testimonial">
                                        </div>
                                        <div class="testi-content">
                                            <p><img  src="{{ asset('assets/images/testimonial/quote/marketing/1.png') }}" alt="testimonial" class="left-img">Extend Accurate Services  Long Term High Impact Experiences Interactiv Streamline Team Compelingly Simplify Solutions Before Techncal Soiund Leadership Skills Creative Holstic Process Imprvents Proactively Streane  Extend Accurate Services  Long Term High Impact Experiences Interactiv Streamline Team Compelingly Simplify Solutions Before Techncal Souind Alternative Niche Markets Forwor Resource<img src="assets/images/testimonial/quote/marketing/02.png" alt="testimonial" class="right-img"></p>
                                            <h6>Somrat Islam  <span>(UI Designer)</span></h6>
                                            <div class="rating">
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="testi-item">
                                    <div class="testi-inner">
                                        <div class="testi-thumb">
                                            <img src="assets/images/team/02.jpg" alt="testimonial">
                                        </div>
                                        <div class="testi-content">
                                            <p><img src="assets/images/testimonial/quote/marketing/01.png" alt="testimonial" class="left-img">Extend Accurate Services  Long Term High Impact Experiences Interactiv Streamline Team Compelingly Simplify Solutions Before Techncal Soiund Leadership Skills Creative Holstic Process Imprvents Proactively Streane  Extend Accurate Services  Long Term High Impact Experiences Interactiv Streamline Team Compelingly Simplify Solutions Before Techncal Souind Alternative Niche Markets Forwor Resource<img src="assets/images/testimonial/quote/marketing/02.png" alt="testimonial" class="right-img"></p>
                                            <h6>Zinat Zaara  <span>(Web Designer)</span></h6>
                                            <div class="rating">
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="testi-item">
                                    <div class="testi-inner">
                                        <div class="testi-thumb">
                                            <img src="assets/images/team/03.jpg" alt="testimonial">
                                        </div>
                                        <div class="testi-content">
                                            <p><img src="assets/images/testimonial/quote/marketing/01.png" alt="testimonial" class="left-img">Extend Accurate Services  Long Term High Impact Experiences Interactiv Streamline Team Compelingly Simplify Solutions Before Techncal Soiund Leadership Skills Creative Holstic Process Imprvents Proactively Streane  Extend Accurate Services  Long Term High Impact Experiences Interactiv Streamline Team Compelingly Simplify Solutions Before Techncal Souind Alternative Niche Markets Forwor Resource<img src="assets/images/testimonial/quote/marketing/02.png" alt="testimonial" class="right-img"></p>
                                            <h6>Rajib Raj  <span>(Web Designer)</span></h6>
                                            <div class="rating">
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="testi-item">
                                    <div class="testi-inner">
                                        <div class="testi-thumb">
                                            <img src="assets/images/team/04.jpg" alt="testimonial">
                                        </div>
                                        <div class="testi-content">
                                            <p><img src="assets/images/testimonial/quote/marketing/01.png" alt="testimonial" class="left-img">Extend Accurate Services  Long Term High Impact Experiences Interactiv Streamline Team Compelingly Simplify Solutions Before Techncal Soiund Leadership Skills Creative Holstic Process Imprvents Proactively Streane  Extend Accurate Services  Long Term High Impact Experiences Interactiv Streamline Team Compelingly Simplify Solutions Before Techncal Souind Alternative Niche Markets Forwor Resource<img src="assets/images/testimonial/quote/marketing/02.png" alt="testimonial" class="right-img"></p>
                                            <h6>Umme Hafsa  <span>(UI Designer)</span></h6>
                                            <div class="rating">
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                                <i class="icofont-star"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- testimonial section ending -->

    <!-- Blog Section Start Here -->
    <section class="blog-section marketing padding-tb">
        <div class="container">
            <div class="section-header">
                <span>Blog</span>
                <h2>Our Latest Blog And News</h2>
            </div>
            <div class="section-wrapper">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="post-item">
                            <div class="post-item-inner">
                                <div class="post-thumb">
                                    <a href="#"><img src="assets/images/blog/01.jpg" alt="lab-blog"></a>
                                </div>
                                <div class="post-content">
                                    <h4><a href="#">Business Ueporting Rouncil Them Could Plan...</a></h4>
                                    <div class="author-date">
                                        <a href="#" class="date"><i class="icofont-calendar"></i>July 12, 2021</a>
                                        <a href="#" class="admin"><i class="icofont-ui-user"></i>Somrat Islam</a>
                                    </div>
                                    <p>Pluoresntly customize pranci an plcentered  customer service anding strategic amerials Interacvely cordinate performe</p>
                                    <div class="post-footer">
                                        <a href="#" class="text-btn">Read More<i class="icofont-double-right"></i></a>
                                        <a href="#" class="comments"><i class="icofont-comment"></i><span>2</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="post-item">
                            <div class="post-item-inner">
                                <div class="post-thumb">
                                    <a href="#"><img src="assets/images/blog/02.jpg" alt="lab-blog"></a>
                                </div>
                                <div class="post-content">
                                    <h4><a href="#">Financial Reporting Qouncil What Could More...</a></h4>
                                    <div class="author-date">
                                        <a href="#" class="date"><i class="icofont-calendar"></i>July 12, 2021</a>
                                        <a href="#" class="admin"><i class="icofont-ui-user"></i>Somrat Islam</a>
                                    </div>
                                    <p>Pluoresntly customize pranci an plcentered  customer service anding strategic amerials Interacvely cordinate performe</p>
                                    <div class="post-footer">
                                        <a href="#" class="text-btn">Read More<i class="icofont-double-right"></i></a>
                                        <a href="#" class="comments"><i class="icofont-comment"></i><span>2</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="post-item">
                            <div class="post-item-inner">
                                <div class="post-thumb">
                                    <a href="#"><img src="assets/images/blog/03.jpg" alt="lab-blog"></a>
                                </div>
                                <div class="post-content">
                                    <h4><a href="#">Consulting Reporting Qounc Arei Could More...</a></h4>
                                    <div class="author-date">
                                        <a href="#" class="date"><i class="icofont-calendar"></i>July 12, 2021</a>
                                        <a href="#" class="admin"><i class="icofont-ui-user"></i>Somrat Islam</a>
                                    </div>
                                    <p>Pluoresntly customize pranci an plcentered  customer service anding strategic amerials Interacvely cordinate performe</p>
                                    <div class="post-footer">
                                        <a href="#" class="text-btn">Read More<i class="icofont-double-right"></i></a>
                                        <a href="#" class="comments"><i class="icofont-comment"></i><span>2</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section Ending Here -->

@endsection



