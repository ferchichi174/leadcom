<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/x-icon/xicon2.png') }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/icofont.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/lightcase.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <title>Leadcom</title>
    @yield('head')
</head>

<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script>





    $(document).ready(function() {
        $('#carouselExampleControls').carousel({
            interval: 4000
        });
        $('#news a').on('click', function() {
            clickEvent = true;
            $('.carousel-item.active').removeClass('active');
            $('#img2').addClass('active');
        });
    });
</script>
<!-- Mobile Menu Start Here -->
<div class="mobile-menu marketing-bg">
    <nav class="mobile-header">
        <div class="header-logo">
            <a href="index-vpn.html"><img src="{{ asset('assets/images/logo/logo.png') }}" alt="logo"></a>
        </div>
        <div class="header-bar">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </nav>
    <nav class="mobile-menu">
        <div class="mobile-menu-area">
            <div class="mobile-menu-area-inner">
                <ul>
                    <li><a href="#">Home</a>
                        <ul>
                            <li><a href="index.html">Smart Seo</a></li>
                            <li><a href="index-host.html">Smart Host</a></li>
                            <li><a href="index-erp.html">Smart Erp</a></li>
                            <li><a href="index-vpn.html">Smart Vpn</a></li>
                            <li><a href="index-pos.html">Smart Pos</a></li>
                            <li class="active"><a href="index-marketing.html">Smart Marketing</a></li>
                            <li><a href="index-shopapp.html">Smart Shopapp</a></li>
                            <li><a href="index-crypto.html">Smart Crypto</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Pages</a>
                        <ul>
                            <li><a href="about.html">About Us</a></li>
                            <li><a href="#">Services</a>
                                <ul>
                                    <li><a href="service.html">Service</a></li>
                                    <li><a href="service-single.html">Service Single</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Team Member</a>
                                <ul>
                                    <li><a href="team.html">Team Style 1</a></li>
                                    <li><a href="team-2.html">Team Style 2</a></li>
                                    <li><a href="team-single.html">Team Single</a></li>
                                </ul>
                            </li>
                            <li><a href="pricing-plan.html">Pricing Plan</a></li>
                            <li><a href="contact-us.html">Contact Us</a></li>
                            <li><a href="comingsoon.html">Coming Soon</a></li>
                            <li><a href="404.html">404</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Portfolio</a>
                        <ul>
                            <li><a href="portfolio.html">Portfolio Style 1</a></li>
                            <li><a href="portfolio-2.html">Portfolio Style 2</a></li>
                            <li><a href="portfolio-single.html">Portfolio Single</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Blog</a>
                        <ul>
                            <li><a href="blog.html">Blog Style 1</a></li>
                            <li><a href="blog-1.html">Blog Style 2</a></li>
                            <li><a href="blog-2.html">Blog Style 3</a></li>
                            <li><a href="blog-single.html">Blog Single</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Shop</a>
                        <ul>
                            <li><a href="shop-page.html">Shop Page</a></li>
                            <li><a href="shop-single.html">Shop Single</a></li>
                            <li><a href="cart-page.html">Cart Page</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<!-- Mobile Menu Ending Here -->


<!-- desktop menu start here -->
<header class="header-section transparent-header marketing-bg">
    <div class="header-area">
        <div class="container">
            <div class="primary-menu">
                <div class="logo">
                    <a href="{{ route('home') }}"><img style =" width: 216px;" src="{{ asset('assets/images/logo/logo.png') }}" alt="logo"></a>
                </div>
                <div class="main-area">
                    <div class="main-menu">
                        <ul>
                            <li><a href="#">Lead</a>
                                <ul>
                                    <li class="active"><a href="{{ route('cmanager') }}">Lead Community manager</a></li>
                                    <li><a href="">lead CRM-ERP</a></li>
                                    <li><a href="">Lead site Vitrine</a></li>
                                    <li><a href="">Lead application</a></li>
                                    <li><a href="">Lead création Graphique</a></li>
                                </ul>
                            </li>

                            <li><a href="#">Services </a>
                                <ul>
                                    <li class="menu-item-has-children"><a href="">Call Center</a>
                                        <ul>
                                            <li><a href="">Service client</a></li>
                                            <li><a href="">Relance des paniers abandonnés </a></li>
                                        </ul></li>
                                    <li><a href="index-erp.html">Acheter un compte</a></li>

                                </ul> </li>


                            <li><a href="#">Conseil de vente</a></li>
                            <li><a href="#">Performance de nos clients</a></li>


                            </li>


                            </li>



                        </ul>
                    </div>
                    <div class="header-btn">
                        <a href="#" class="lab-btn"><span>Commander du Lead. </span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- desktop menu ending here -->
@yield('content')


<!-- Footer Section Start Here -->
<footer>
    <div class="footer-top style-2 maketing padding-tb">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="footer-item first-set">
                        <div class="footer-inner">
                            <div class="footer-content">
                                <div class="title">
                                    <h6>About SmratMarketing</h6>
                                </div>
                                <div class="content">
                                    <p>We believe in Simple Creative & Flexible Design Standards.</p>
                                    <h6>Headquarters:</h6>
                                    <p>795 Folsom Ave, Suite 600 San Francisco, CA 94107</p>
                                    <ul>
                                        <li>
                                            <p><span>Phone:</span>(91) 8547 632521</p>
                                        </li>
                                        <li>
                                            <p><span>Email:</span><a href="#">info@gmail.com</a></p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="footer-item">
                        <div class="footer-inner">
                            <div class="footer-content">
                                <div class="title">
                                    <h6>Theme Features</h6>
                                </div>
                                <div class="content">
                                    <ul>
                                        <li><a href="#">Documentation</a></li>
                                        <li><a href="#">Feedback</a></li>
                                        <li><a href="#">Plugins</a></li>
                                        <li><a href="#">Support Forums</a></li>
                                        <li><a href="#">Themes</a></li>
                                        <li><a href="#">WordPress Blog</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="footer-item">
                        <div class="footer-inner">
                            <div class="footer-content">
                                <div class="title">
                                    <h6>Social Contact</h6>
                                </div>
                                <div class="content">
                                    <ul>
                                        <li><a href="#"><i class="icofont-facebook"></i>Facebook</a></li>
                                        <li><a href="#"><i class="icofont-twitter"></i>Twitter</a></li>
                                        <li><a href="#"><i class="icofont-instagram"></i>Instagram</a></li>
                                        <li><a href="#"><i class="icofont-youtube"></i>YouTube</a></li>
                                        <li><a href="#"><i class="icofont-xing"></i>Xing</a></li>
                                        <li><a href="#"><i class="icofont-github"></i>Github</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-12">
                    <div class="footer-item">
                        <div class="footer-inner">
                            <div class="footer-content">
                                <div class="title">
                                    <h6>Our Support</h6>
                                </div>
                                <div class="content">
                                    <ul>
                                        <li><a href="#">Help Center</a></li>
                                        <li><a href="#">Paid with Mollie</a></li>
                                        <li><a href="#">Status</a></li>
                                        <li><a href="#">Changelog</a></li>
                                        <li><a href="#">Contact Support</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom style-2">
        <div class="container">
            <div class="section-wrapper">
                <p>&copy; 2021 All Rights Reserved. Designed by <a href="#">CodexCoder</a></p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section Ending Here -->

<!-- scrollToTop start here -->
<a href="#" class="scrollToTop"><i class="icofont-swoosh-up"></i><span class="pluse_1"></span><span class="pluse_2"></span></a>
<!-- scrollToTop ending here -->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<script src="{{ asset('assets/js/jquery.js') }}"></script>
<script src="{{ asset('assets/js/fontawesome.min.js') }}"></script>
<script src="{{ asset('assets/js/waypoints.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/lightcase.js') }}"></script>
<script src="{{ asset('assets/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/js/swiper.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('assets/js/progress.js') }}"></script>
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('assets/js/functions.js') }}"></script>
<script>
    $(window).scroll(function() {
        var hT = $('.skill-bar-wrapper').offset().top,
            hH = $('.skill-bar-wrapper').outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop();
        if (wS > (hT+hH-1.4*wH)){
            jQuery(document).ready(function(){
                jQuery('.skillbar-container').each(function(){
                    jQuery(this).find('.skills').animate({
                        width:jQuery(this).attr('data-percent')
                    }, 5000); // 5 seconds
                });
            });
        }
    });


</script>
<script>





    $(document).ready(function() {
        $('#carouselExampleControls').carousel({
            interval: 4000
        });
        $('#news a').on('click', function() {
            clickEvent = true;
            $('.carousel-item').removeClass('active');
            $('#img2').addClass('active');
        });
    });
</script>

</body>
</html>
