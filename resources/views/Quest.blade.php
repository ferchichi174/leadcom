<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Quiz</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/quest/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/quest/assets/css/owl.carousel.css">
    <link rel="stylesheet" href="/quest/https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/quest/assets/css/animate.min.css">
    <link rel="stylesheet" href="/quest/assets/css/fontawesome-all.css">
    <link rel="stylesheet" href="/quest/assets/css/style.css">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/x-icon/xicon2.png') }}">


<style>

    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap');

    * {
        margin: 0;
        padding: 0;
        font-family: "Poppins", sans-serif;
    }

    #rangeValue,#rangeValue2,#rangeValue3,#rangeValue4  {
        position: relative;
        display: block;
        text-align: center;
        font-size: 6em;
        color: #999;
        font-weight: 400;
    }
    .range {
        width: 800px;
        height: 15px;
        -webkit-appearance: none;
        background: #111;
        outline: none;
        border-radius: 15px;
        overflow: hidden;
        box-shadow: inset 0 0 5px rgba(0, 0, 0, 1);
    }
    .range::-webkit-slider-thumb {
        -webkit-appearance: none;
        width: 15px;
        height: 15px;
        border-radius: 50%;
        background: #33abe5;
        cursor: pointer;
        border: 4px solid #333;
        box-shadow: -407px 0 0 400px #33abe5;
    }
</style>
</head>
<body>

<div class="quiz-top-area text-center" style="background-color: #fffeff">
    <div class="logo">
        <a href="{{ route('home') }}"><img style =" width: 216px;float: left;margin-left: 100px;"  src="{{ asset('assets/images/logo/logo.png') }}" alt="logo"></a>
    </div>
    <h1 style="margin-right:495px ;color:white;"> Analyse de vos leads.</h1>
</div>
<div class="wrapper position-relative">
    <div class="wizard-content-1 clearfix">
        <div class="steps d-inline-block position-absolute clearfix">
            <ul class="tablist multisteps-form__progress">
                <li class="multisteps-form__progress-btn js-active current"></li>
                <li class="multisteps-form__progress-btn "></li>
                <li class="multisteps-form__progress-btn"></li>
                <li class="multisteps-form__progress-btn"></li>
                <li class="multisteps-form__progress-btn"></li>
            </ul>
        </div>
        <div class="step-inner-content clearfix position-relative">
            <form class="multisteps-form__form" action="{{ route('resultat') }}" id="wizard" method="POST">
                @csrf
                <div class="form-area position-relative">


                    <div class="multisteps-form__panel  js-active" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 1</span>
                                <h2>Vous êtes ?</h2>
                            </div>
                            <div class="quiz-option-selector clearfix" style="max-width: 1496px;margin-top: 100px;" >
                                <ul>
                                    <li style="width: 33%;">
                                        <label class="start-quiz-item">
                                            <input type="radio" name="quiz" value="Une agence Web" class="exp-option-box">
                                            <span class="exp-number text-uppercase">A</span>
                                            <span class="exp-label">Une agence Web</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li style="width: 33%;">
                                        <label>
                                            <input type="radio" name="quiz" value="Une agence Marketing" class="exp-option-box">
                                            <span class="exp-number text-uppercase">b</span>
                                            <span class="exp-label">Une agence Marketing</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li style="width: 33%;">
                                        <label>
                                            <input type="radio" name="quiz" value="Agence de Communication " class="exp-option-box">
                                            <span class="exp-number text-uppercase">c</span>
                                            <span class="exp-label">Agence de Communication </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>

                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="quest/assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 20%">
                                        <span>24% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li class="d-none"><span class="js-btn-next" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 2</span>
                                <h2>Avez-vous déjà commander du lead chez un fournisseur dans le secteur du digital pour vos équipe commercial ? </h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="fournisseur" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" alt="" style="max-width: 57px;"></span>
                                            <span class="exp-label">Oui </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="fournisseur" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" style="max-width: 57px;" alt=""></span>
                                            <span class="exp-label">Non</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>


                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- step 1 -->

                    <!-- step 2 -->
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms">
                            <div class="quiz-title text-center">
                                <span>Question 3</span>
                                <h2>Comment aujourd’hui votre entreprise trouve de nouveaux contrat ?</h2>
                            </div>
                            <div class="quiz-option-selector">
                                <label class="quiz-details-option position-relative">
                                    <input type="radio" name="contrat" value="Email Markering" class="exp-option-box">
                                    <span class="select-area"></span>
                                    <span>Vous demarchez téléphoniquement des listes de prospects, travailler sur du lead qualifier ou achetez votre lead.</span>
                                </label>
                                <label class="quiz-details-option position-relative">
                                    <input type="radio" name="contrat" value="Email Markering" class="exp-option-box">
                                    <span class="select-area"></span>
                                    <span>Vous communiquer sur les réseaux sociaux tel que Linkedin,Facebook, instagram sponsorisés vos services en lignes pour géneré des clients</span>
                                </label>
                                <label class="quiz-details-option position-relative">
                                    <input type="radio" name="contrat" value="Email Markering" class="exp-option-box">
                                    <span class="select-area"></span>
                                    <span>Vous rédigez des articles ou livre blanc pour genéré des prospects organisé des campagnes marketing</span>
                                </label>
                                <label class="quiz-details-option position-relative">
                                    <input type="radio" name="contrat" value="Email Markering" class="exp-option-box">
                                    <span class="select-area"></span>
                                    <span>autre</span>
                                </label>
                            </div>
                            <div class="bottom-vector">
                                <img src="quest/assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 60%">
                                        <span>60% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- step 3 -->
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 4</span>
                                <h2>Quelle sont les outils que vous utilisez pour obtenir les informations de vos prospect ?</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="outils" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi1.png" alt=""></span>
                                            <span class="exp-label">Scraping </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="outils" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi2.png" alt=""></span>
                                            <span class="exp-label">Réseaux sociaux</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="outils" value="Digital Marketing" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi3.png" alt=""></span>
                                            <span class="exp-label">Prospection téléphonique</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="outils" value="SEO" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi4.png" alt=""></span>
                                            <span class="exp-label">INBOUD stratégie</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="outils" value="SEO" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi7.png" alt=""></span>
                                            <span class="exp-label">Autre </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="outils" value="SEO" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi6.png" alt=""></span>
                                            <span class="exp-label">Aucun</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 5</span>
                                <h2>Quel quantité de prospect traité vous et vos équipe commercial ?</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <div style="margin-bottom: 150px;">
                                    <span id="rangeValue4">0 </span>
                                    <Input id="q5" style=" margin-left: 171px;" class="range" type="range" name ="quantite" value="0" min="0" max="100000" onChange="rangeSlide4(this.value)" onmousemove="rangeSlide4(this.value)"></Input>
                                </div>
                                <script type="text/javascript">
                                    function rangeSlide4(value) {
                                        document.getElementById('rangeValue4').innerHTML = value ;
                                    }
                                </script>

                            </div>


                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- step 4 -->
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 6</span>
                                <h2>Quelle est le taux de conversion actuel de vos campagnes commerciales ?</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <div style="margin-bottom: 150px;">
                                    <span id="rangeValue">0 %</span>
                                    <Input  id = "q6" style=" margin-left: 171px;" class="range" type="range" name ="taux" value="0" min="0" max="100" onChange="rangeSlide(this.value)" onmousemove="rangeSlide(this.value)"></Input>
                                </div>
                                <script type="text/javascript">
                                    function rangeSlide(value) {
                                        document.getElementById('rangeValue').innerHTML = value+"%" ;
                                    }


                                </script>

                            </div>


                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 12</span>
                                <h2>Combien charger vous votre prestation.</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <div style="margin-bottom: 150px;">
                                    <span id="rangeValue3">0 %</span>
                                    <Input id="q12" onclick="myFunction()" style=" margin-left: 171px;" class="range" type="range" name="prestation" value="0" min="0" max="5000" onChange="rangeSlide3(this.value)" onmousemove="rangeSlide3(this.value)"></Input>
                                </div>
                                <script type="text/javascript">
                                    function rangeSlide3(value) {
                                        document.getElementById('rangeValue3').innerHTML = value+"€" ;
                                    }
                                    function myFunction() {
                                        var q5 =document.getElementById("q5").value;
                                        var q6 =document.getElementById("q6").value;
                                        var q12 =document.getElementById("q12").value;
                                        var chiffreA = ((q5 * q6)/100)* q12;
                                        var chiffreAff = (parseInt(chiffreA));
                                            var chi = chiffreAff /1000;
                                        document.getElementById("q8").setAttribute("min",parseInt(chi));
                                    }

                                </script>

                            </div>


                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>

                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- step 5 -->
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 7</span>
                                <h2>Dans quelle zone ciblée vous pour vos prospects ?</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix" style="max-width: 1496px;margin-top: 100px;">
                                <ul>
                                    <li style="width: 33%;">
                                        <label class="start-quiz-item">
                                            <input type="radio" name="zone" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="quest/assets/img/colombia.png" alt="" style="max-width: 57px;"></span>
                                            <span class="exp-label">Régional </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li style="width: 33%;">
                                        <label>
                                            <input type="radio" name="zone" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="quest/assets/img/citizenship.png" style="max-width: 57px;" alt=""></span>
                                            <span class="exp-label">Territorial</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li style="width: 33%;">
                                        <label>
                                            <input type="radio" name="zone" value="Digital Marketing" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="quest/assets/img/international.png" alt="" style="max-width: 57px;"></span>
                                            <span class="exp-label">Mondial </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>

                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- step 6 -->
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 8</span>
                                <h2>Quelle sont vos objectif de vente pour vos équipe dans le mois ?</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <div style="margin-bottom: 150px;">
                                    <span id="rangeValue2">0 k €</span>
                                    <Input id="q8" style="  margin-left: 171px;" class="range" type="range" name="objectif" value="0" min="0" max="100000" onChange="rangeSlide2(this.value)" onmousemove="rangeSlide2(this.value)"></Input>
                                </div>
                                <script type="text/javascript">
                                    function rangeSlide2(value) {
                                        document.getElementById('rangeValue2').innerHTML = value+"k €" ;
                                    }
                                </script>

                            </div>


                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- step 7 -->
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 9</span>
                                <h2>Avez-vous établi le persona de vos prospect ?</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="persona" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" alt="" style="max-width: 57px;"></span>
                                            <span class="exp-label">Oui </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="persona" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" style="max-width: 57px;" alt=""></span>
                                            <span class="exp-label">Non</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>


                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 10</span>
                                <h2>D’où provienne la majeure partie de vos leads ? </h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="partie" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img style="max-width: 42px;" src="quest/assets/img/telephone.png" alt=""></span>
                                            <span class="exp-label">Téléphone </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="partie" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img style="max-width: 42px;" src="quest/assets/img/social-media.png" alt=""></span>
                                            <span class="exp-label">Réseaux sociaux</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="partie" value="Digital Marketing" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img style="max-width: 42px;" src="quest/assets/img/seo-search-symbol.png" alt=""></span>
                                            <span class="exp-label">Référencement naturel</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="partie" value="SEO" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img style="max-width: 42px;" src="quest/assets/img/swear.png" alt=""></span>
                                            <span class="exp-label">ParrainageBouche à oreille</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms">
                            <div class="quiz-title text-center">
                                <span>Question 11</span>
                                <p>Quelle type de service numérique votre agence fournis ?</p>
                            </div>
                            <div class="quiz-option-selector">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="quiz-option-area position-relative">
                                            <input type="radio" name="service" value="SEO" class="exp-option-box">
                                            <span class="quiz-option-img">
														<img src="quest/assets/img/photo-2.png" alt="">
													</span>
                                            <span class="quiz-option-serial text-center">1</span>
                                            <span class="quiz-option-text">Design graphique</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="quiz-option-area position-relative">
                                            <input type="radio" name="service" value="SEO" class="exp-option-box">
                                            <span class="quiz-option-img">
														<img src="quest/assets/img/photo-3.png" alt="">
													</span>
                                            <span class="quiz-option-serial text-center">2</span>
                                            <span class="quiz-option-text">Développement application Mobile</span>
                                        </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="quiz-option-area position-relative">
                                            <input type="radio" name="service" value="SEO" class="exp-option-box">
                                            <span class="quiz-option-img">
														<img src="quest/assets/img/photo-1.png" alt="">
													</span>
                                            <span class="quiz-option-serial text-center">3</span>
                                            <span class="quiz-option-text">Développement web </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="bottom-vector">
                                <img src="quest/assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 80%">
                                        <span>80% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>

                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>



                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 13</span>
                                <h2>Commandez-vous du lead actuellement ?</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="lead" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" alt="" style="max-width: 57px;"></span>
                                            <span class="exp-label">Oui </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="lead" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" style="max-width: 57px;" alt=""></span>
                                            <span class="exp-label">Non</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>


                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 14</span>
                                <h2>Qu’elle est la fréquence de consommation de votre lead ?</h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="freq" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi1.png" alt=""></span>
                                            <span class="exp-label">Commande journalière </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="freq" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi2.png" alt=""></span>
                                            <span class="exp-label">Commande Hebdomadaire</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="freq" value="Digital Marketing" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi3.png" alt=""></span>
                                            <span class="exp-label">Commande mensuel</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="freq" value="SEO" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="assets/img/qi4.png" alt=""></span>
                                            <span class="exp-label">Commande Annuel</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>

                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 15</span>
                                <h2>Cherchez vous une solution pour générer plus de lead qualifier ? </h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="solution" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" alt="" style="max-width: 57px;"></span>
                                            <span class="exp-label">Oui </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="solution" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" style="max-width: 57px;" alt=""></span>
                                            <span class="exp-label">Non</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>


                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                <ul>
                                    <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                    <li><span class="js-btn-next" title="NEXT">Next Question</span></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="multisteps-form__panel" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative">
                            <div class="quiz-title text-center">
                                <span>Question 16</span>
                                <h2>Souhaité vous commander et obtenir 10 leads qualifier gratuitement pour votre première commande ?  </h2>
                            </div>
                            <div class="quiz-option-selector quiz-option-selector-2 clearfix">
                                <ul>
                                    <li>
                                        <label class="start-quiz-item">
                                            <input type="radio" name="quiz" value="Email Markering" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" alt="" style="max-width: 57px;"></span>
                                            <span class="exp-label">Oui </span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label>
                                            <input type="radio" name="quiz" value="Font Developer" class="exp-option-box">
                                            <span class="exp-number text-uppercase"><img src="" style="max-width: 57px;" alt=""></span>
                                            <span class="exp-label">Non</span>
                                            <span class="checkmark-border"></span>
                                        </label>
                                    </li>


                                </ul>
                            </div>
                            <div class="bottom-vector">
                                <img src="assets/img/bq1.png" alt="">
                            </div>
                            <div class="quiz-progress-area">
                                <div class="progress">
                                    <div class="progress-bar position-relative" style="width: 40%">
                                        <span>40% complete, keep it up!</span>
                                    </div>
                                </div>
                            </div>
                            <div class="actions clearfix">
                                                    <ul>
                                                        <li><span class="js-btn-prev" title="PREV">Previous Question</span></li>
                                                        <li><button class="js-btn-submit" type="submit"><span>SUBMIT</span></button></li>
                                                    </ul>
                                                </div>

                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
</div>


<script src="quest/assets/js/jquery-3.3.1.min.js"></script>
<script src="quest/assets/js/bootstrap.min.js"></script>
<script src="quest/assets/js/popper.min.js"></script>
<script src="quest/assets/js/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="quest/assets/js/form-step.js"></script>
<script src="quest/assets/js/jquery.validate.min.js"></script>
<script src="quest/assets/js/main.js"></script>

<!-- <script src="assets/js/switch.js"></script> -->

</body>

</html>
