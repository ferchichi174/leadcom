<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Polls Result</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('quest1/assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('quest1/assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('quest1/assets/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{asset('quest1/assets/css/style.css')}}">

<body>
<div class="wrapper">
    <div class="poll-top-head text-center">
        <h1>Polls Result</h1>
    </div>
    <div class="poll-top-title text-center">
        <h2>If you write a book about Failure
            and it doesn’t sell, is it called Success</h2>
    </div>
    <!-- /poll top content -->
    <div class="poll-content-img clearfix">
        <div class="poll-result-circle-percent float-left position-relative">
            <div class="pr-value-text">Lead</div>
            <div class="first progress_area position-relative"><strong id="st">{{ $res }}</strong></div>
        </div>
        <div class="poll-result-text">
            <h3>Nombre de leads pour atteindre vos objectif</h3>
            <a href="">Change Answer</a>
        </div>
    </div>

</div>
<!-- /wrapper -->
<script src="{{asset('quest1/assets/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('quest1/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('quest1/assets/js/popper.min.js')}}"></script>
<script src="{{asset('quest1/assets/js/appear.js')}}"></script>
<script src="{{asset('quest1/assets/js/circle-progress.js')}}"></script>
<script src="{{asset('quest1/assets/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('quest1/assets/js/main.js')}}"></script>
</body>

</html>

