<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Cost Calculator and Order Wizard">
    <meta name="author" content="UWS">
    <title>Leadcom</title>

    <!-- Favicon -->
    <link href="img/favicon.png" rel="shortcut icon">

    <!-- Google Fonts - Poppins, Karla -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Karla:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome CSS -->
    <link href="{{ asset("vendor/fontawesome/css/all.min.css") }}" rel="stylesheet">

    <!-- Custom Font Icons -->
    <link href="{{ asset("vendor/icomoon/css/iconfont.min.css") }}" rel="stylesheet">

    <!-- Vendor CSS -->
    <link href="{{ asset("vendor/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet">
    <link href="{{ asset("vendor/animate/css/animate.min.css") }}" rel="stylesheet">
    <link href="{{ asset("vendor/dmenu/css/menu.css") }}" rel="stylesheet">
    <link href="{{ asset("vendor/hamburgers/css/hamburgers.min.css") }}" rel="stylesheet">
    <link href="{{ asset("vendor/mmenu/css/mmenu.min.css") }}" rel="stylesheet">
    <link href="{{ asset("vendor/range-slider/css/ion.rangeSlider.css") }}" rel="stylesheet">
    <link href="{{ asset("vendor/magnific-popup/css/magnific-popup.css") }}" rel="stylesheet">
    <link href="{{ asset("vendor/float-labels/css/float-labels.min.css") }}" rel="stylesheet">

    <!-- Main CSS -->
    <link href="{{ asset("css/style-blue.css") }}" rel="stylesheet">

</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div data-loader="circle-side"></div>
</div>
<!-- Preloader End -->

<!-- Page -->
<div id="page">
    <!-- Header -->


    <header class="main-header sticky">
        <a href="#menu" class="btn-mobile">
            <div class="hamburger hamburger--spin" id="hamburger">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
        </a>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div id="logo">
                        <a href="{{ route('home') }}"><img style =" width: 216px;" src="{{ asset('assets/images/logo/logo.png') }}" alt="logo"></a>
                    </div>
                </div>
                <div class="col-lg-9 col-6">
                    <ul id="menuIcons">
                        <li><a href="#"><i class="icon icon-support"></i></a></li>
                        <li><a href="#"><i class="icon icon-shopping-cart2"></i></a></li>
                    </ul>
                    <!-- Menu -->
                    <nav id="menu" class="main-menu">
                        <ul>
                            {{--                            <li><span><a href="index.html">Home</a></span></li>--}}
                            <li>
                                <span><a href="#">Lead <i class="fa fa-chevron-down"></i></a></span>
                                <ul>
                                    <li><a href="{{ route('cost',['id'=>5,'gid'=>2]) }}">Lead community manager</a></li>
                                    <li><a href="{{ route('cost',['id'=>2,'gid'=>2]) }}">Lead CRM- ERP</a></li>
                                    <li><a href="{{ route('cost',['id'=>1,'gid'=>2]) }}"> Lead site Vitrine</a></li>
                                    <li><a href="{{ route('cost',['id'=>3,'gid'=>2]) }}"> Lead application</a></li>
                                    <li><a href="{{ route('cost',['id'=>4,'gid'=>2]) }}"> Lead E-commerce</a></li>
                                    <li><a href="{{ route('cost',['id'=>6,'gid'=>2]) }}"> Lead création Graphique</a></li>
                                </ul>
                            </li>
                            <li>
                                <span><a href="#">Service<i class="fa fa-chevron-down"></i></a></span>
                                <ul>
                                    <li><a href="php/phpmailer/email.html" target="_blank">Call center</a></li>
                                    <li>

                                    <li><a href="javascript:;" id="openSimpleMailSummaryImage">Acheter un compte</a></li>
                                </ul>
                            </li>
                            <li><span><a href="faq.html">Faq</a></span></li>
                            <li><span><a href="contacts.html">Contacts</a></span></li>
                        </ul>
                    </nav>
                    <!-- Menu End -->
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Sub Header -->
    <div class="sub-header">
        <div class="container">
            <h1>Lead  {{ $data->name }}</h1>
        </div>
    </div>
    <!-- Sub Header End -->

    <!-- Main -->
    <main>
        <!-- Order  -->
        <div class="order">
            <div class="container">
                <!-- Form -->
                <form method="POST" id="orderForm" name="orderForm" action="php/send_order_3_attached_pdf.php">
                    <div class="row">
                        <div class="col-lg-8" id="mainContent">
                            <div id="optionGroup2" class="row option-box">

                                <div class="option-box-header">
                                    <h3>Option Group 1</h3>
                                    <p>
                                        Subtitle or short description can be set here.
                                        <a href="javascript:;" id="openGallery1" class="option-box-link">Gallery</a>
                                    </p>
                                </div>
                                <div class="col-12">

                                    <select id="optionGroup2List" class="wide price-list" name="optionGroup2List">
                                        <option value="0">Select</option>
                                        <option  value="France">France</option>
                                        <option value="Canada">Canada</option>
                                        <option value="Dubaï">Dubaï</option>
                                        <option value="Suisse">Suisse</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Calculator Area -->
                            <div id="optionGroup1" class="row option-box">
                                <span class="price-box-desc">From</span>
                                <div id="boxprice" class="price-box"><sup>€</sup>2.50</div>
                                <div class="option-box-header">
                                    <h3>Option Group 1</h3>
                                    <p>
                                        Subtitle or short description can be set here.
                                        <a href="javascript:;" id="openGallery1" class="option-box-link">Gallery</a>
                                    </p>
                                </div>
                                <div class="col-12">
                                    <input type="hidden" id="option1Title" name="option1Title" value="Select" />
                                    <input type="hidden" id="option1Price" name="option1Price" value="0" />
                                    <select id="optionGroup1List" class="wide price-list" name="optionGroup1List">
                                        <option value="0">Select</option>
                                        <option  value="{{ number_format((float)$offre->offre1, 2, '.', '') }}">Lead standart</option>
                                        <option value="{{ number_format((float)$offre->offre2, 2, '.', '') }}" >Lead medium</option>
                                        <option value="{{ number_format((float)$offre->offre3, 2, '.', '') }}">Lead premium</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row option-box">

                                <div class="option-box-header">
                                    <h3>Quantity Slider</h3>
                                    <p>Subtitle or short description can be set here.</p>
                                </div>
                                <div class="col-12" id ="mrigel" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup1RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel1" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup2RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty1" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel2" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup3RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty2" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12" id ="mrigel4" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup4RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty4" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel5" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup5RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty5" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel6" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup6RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty6" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel7" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup7RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty7" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel8" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup8RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty8" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel9" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup9RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty9" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel10" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup10RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty10" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel11" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup11RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty11" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel12" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup12RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty12" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel13" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup13RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty13" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel14" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup14RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty14" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id ="mrigel15" style="display:none ;" >
                                    <div class="row">
                                        <div class="col-md-10" >
                                            <input id="optionGroup15RangeSlider" type="text" value="" />
                                        </div>
                                        <div class="col-md-2">
                                            <input id="optionGroup1Qty15" type="text" class="qty-input form-control" name="optionGroup1Qty" value="0" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="extraOptionGroup1" class="row option-box">
                                <div class="price-box"><sup>$</sup>50</div>
                                <div class="option-box-header">
                                    <h3>Extra Option 1</h3>
                                    <p>Subtitle or short description can be set here.</p>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <input type="hidden" name="extraOption1Title" value="Extra Option 1" />
                                    <input type="checkbox" id="extraOption1" class="inp-cbx" name="extraOption1" value="50.00" />


                                    {{--                                            <select class="selectpicker" multiple data-live-search="true">--}}
                                    {{--                                                @foreach($data->activities as $act)--}}
                                    {{--                                                <option>{{$act->name}}</option>--}}

                                    {{--                                                @endforeach--}}
                                    {{--                                            </select>--}}



                                    <select class="aaa" name="field2" id="field2" multiple multiselect-search="true" multiselect-select-all="true" multiselect-max-items="3" onchange="console.log(this.selectedOptions)">
                                        @foreach($data->activities as $act)
                                            <option>{{$act->name}}</option>
                                        @endforeach
                                    </select>


                                </div>
                            </div>
                            <!-- Calculator Area End -->
                            <!-- Personal Details -->
                            <div id="personalDetails" class="row contact-box">
                                <div class="contact-box-header">
                                    <h3>Contact Details</h3>
                                    <p>Please give VALID email to check the result.</p>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="userName">Full Name</label>
                                        <input id="userName" class="form-control" name="username" type="text" data-parsley-pattern="^[a-zA-Z\s.]+$" required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input id="email" class="form-control" name="email" type="email" required />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Phone e.g.: +12345</label>
                                        <input id="phone" class="form-control" name="phone" type="text" data-parsley-pattern="^\+{1}[0-9]+$" />
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <input id="address" class="form-control" name="address" type="text" data-parsley-pattern="^[,.a-zA-Z0-9\s.]+$" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="inputMessage">Message</label>
                                        <textarea class="form-control" id="inputMessage" name="message" data-parsley-pattern="^[a-zA-Z0-9\s.:,!?']+$"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- Personal Details End -->
                        </div>
                        <div class="col-lg-4" id="sidebar">
                            <!-- Order Container -->
                            <div id="orderContainer" class="theiaStickySidebar">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" id="subSum1" name="subSum1" value="0" />
                                        <input type="hidden" id="subSum2" name="subSum2" value="0" />
                                        <input type="hidden" id="subSum3" name="subSum3" value="0" />
                                        <input type="hidden" id="totalDue" name="totalDue" value="0" />
                                        <h3>Order Summary</h3>
                                        <ul id="orderSumList">
                                            <li id="optionGroup1Sum"></li>
                                            <li id="optionGroup2Sum"></li>
                                            <li id="optionGroup3Sum"></li>
                                            <li id="extraOption1Sum"></li>
                                            <li id="extraOption2Sum"></li>
                                        </ul>
                                        <div class="row total-container">
                                            <div class="col-6 p-0">
                                                <input type="text" id="totalTitle" class="summaryInput" name="totallabel" value="" disabled />
                                            </div>
                                            <div class="col-6 p-0">
                                                <input type="text" id="total" class="summaryInput" name="total" value="0" data-parsley-errors-container="#totalError" data-parsley-empty-order=""
                                                       disabled />
                                            </div>
                                        </div>
                                        <div id="totalError"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="checkbox" id="cbx" class="inp-cbx" name="terms" value="yes" required />
                                            <label class="cbx terms" for="cbx">
													<span>
														<svg width="12px" height="10px" viewbox="0 0 12 10">
															<polyline points="1.5 6 4.5 9 10.5 1"></polyline>
														</svg>
													</span>
                                                <span>Accept<a href="pdf/terms.pdf" class="terms-link" target="_blank">Terms</a>.</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="submit" name="submit" class="btn-form-func">
                                            <span class="btn-form-func-content">SUBMIT</span>
                                            <span class="icon"><i class="fa fa-check" aria-hidden="true"></i></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="button" name="reset" id="resetBtn" class="btn-form-func btn-form-func-alt-color">
                                            <span class="btn-form-func-content">RESET</span>
                                            <span class="icon"><i class="fa fa-times" aria-hidden="true"></i></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="row footer">
                                    <div class="col-lg-12 text-center">
                                        <small>&copy; 2020 Your Company.</small>
                                    </div>
                                </div>
                            </div>
                            <!-- Order Container End -->
                        </div>
                    </div>
                </form>
                <!-- Form End -->
            </div>
        </div>
        <!-- Order End -->
    </main>
    <!-- Main End -->

    <!-- Footer -->
    <footer class="main-footer">
        {{--        <div class="container">--}}
        {{--            <div class="row">--}}
        {{--                <div class="col-md-3">--}}
        {{--                    <h5 class="footer-heading">Menu Links</h5>--}}
        {{--                    <ul class="list-unstyled nav-links">--}}
        {{--                        <li><i class="fas fa-angle-right"></i> <a href="index.html" class="footer-link">Home</a></li>--}}
        {{--                        <li><i class="fas fa-angle-right"></i> <a href="faq.html" class="footer-link">FAQ</a></li>--}}
        {{--                        <li><i class="fas fa-angle-right"></i> <a href="contacts.html" class="footer-link">Contacts</a></li>--}}
        {{--                    </ul>--}}
        {{--                </div>--}}
        {{--                <div class="col-md-3">--}}
        {{--                    <h5 class="footer-heading">Order Wizard</h5>--}}
        {{--                    <ul class="list-unstyled nav-links">--}}
        {{--                        <li><i class="fas fa-angle-right"></i> <a href="calculator-1.html" class="footer-link">Calculator Demo 1</a></li>--}}
        {{--                        <li><i class="fas fa-angle-right"></i> <a href="calculator-2.html" class="footer-link">Calculator Demo 2</a></li>--}}
        {{--                        <li><i class="fas fa-angle-right"></i> <a href="calculator-3.html" class="footer-link">Calculator Demo 3</a></li>--}}
        {{--                    </ul>--}}
        {{--                </div>--}}
        {{--                <div class="col-md-4">--}}
        {{--                    <h5 class="footer-heading">Contacts</h5>--}}
        {{--                    <ul class="list-unstyled contact-links">--}}
        {{--                        <li><i class="icon icon-map-marker"></i><a href="https://goo.gl/maps/vKgGyZe2JSRLDnYH6" class="footer-link" target="_blank">Address: 1234 Street Name, City Name, USA</a>--}}
        {{--                        </li>--}}
        {{--                        <li><i class="icon icon-envelope3"></i><a href="mailto:info@yourdomain.com" class="footer-link">Mail: info@yourdomain.com</a></li>--}}
        {{--                        <li><i class="icon icon-phone2"></i><a href="tel:+3630123456789" class="footer-link">Phone: +3630123456789</a></li>--}}
        {{--                    </ul>--}}
        {{--                </div>--}}
        {{--                <div class="col-md-2">--}}
        {{--                    <h5 class="footer-heading">Follow Us</h5>--}}
        {{--                    <ul class="list-unstyled social-links">--}}
        {{--                        <li><a href="https://facebook.com" class="social-link" target="_blank"><i class="icon icon-facebook"></i></a></li>--}}
        {{--                        <li><a href="https://twitter.com" class="social-link" target="_blank"><i class="icon icon-twitter"></i></a></li>--}}
        {{--                        <li><a href="https://instagram.com" class="social-link" target="_blank"><i class="icon icon-instagram"></i></a></li>--}}
        {{--                        <li><a href="https://pinterest.com" class="social-link" target="_blank"><i class="icon icon-pinterest"></i></a></li>--}}
        {{--                    </ul>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <hr>--}}
        {{--            <div class="row">--}}
        {{--                <div class="col-md-8">--}}
        {{--                    <ul id="subFooterLinks">--}}
        {{--                        <li><a href="https://themeforest.net/user/ultimatewebsolutions" target="_blank">With <i class="fa fa-heart pulse"></i> by UWS</a></li>--}}
        {{--                        <li><a href="pdf/terms.pdf" target="_blank">Terms and conditions</a></li>--}}
        {{--                        <li><a href="faq.html">Faq</a></li>--}}
        {{--                    </ul>--}}
        {{--                </div>--}}
        {{--                <div class="col-md-4">--}}
        {{--                    <div id="copy">© 2021 Costy</div>--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </footer>
    <!-- Footer End -->
</div>
<!-- Page End -->

<!-- Modal Popup Alert 1 -->
<div class="modal" id="alertModal1" tabindex="-1" role="dialog" aria-labelledby="alertModal1Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="alertModal1Title">Attention</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="icon icon-cross"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <p>Nous vous proposons de passé sur une l’offre <span class="alert-text-strong">Lead medium</span> Plus intéressante et plus adapté à votre Agence</p>
            </div>
            <div class="modal-footer">
                <button class="btn-modal" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="alertModal2" tabindex="-1" role="dialog" aria-labelledby="alertModal2Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="alertModal2Title">Attention</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="icon icon-cross"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <p>Nous vous proposons de passé sur une l’offre <span class="alert-text-strong">Lead premium</span> Plus intéressante et plus adapté à votre Agence</p>
            </div>
            <div class="modal-footer">
                <button class="btn-modal" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Back to top button -->
<div id="toTop"><i class="fa fa-chevron-up"></i></div>

<!-- Vendor Javascript Files -->
<script src="{{ asset("vendor/jquery/jquery.min.js") }}"></script>
<script src="{{ asset("vendor/bootstrap/js/bootstrap.min.js") }}"></script>
<script src="{{ asset("vendor/easing/js/easing.min.js") }}"></script>
<script src="{{ asset("vendor/parsley/js/parsley.min.js") }}"></script>
<script src="{{ asset("vendor/nice-select/js/jquery.nice-select.min.js") }}"></script>
<script src="{{ asset("vendor/range-slider/js/ion.rangeSlider.min.js") }}"></script>
<script src="{{ asset("vendor/price-format/js/jquery.priceformat.min.js") }}"></script>
<script src="{{ asset("vendor/theia-sticky-sidebar/js/ResizeSensor.min.js") }}"></script>
<script src="{{ asset("vendor/theia-sticky-sidebar/js/theia-sticky-sidebar.min.js") }}"></script>
<script src="{{ asset("vendor/mmenu/js/mmenu.min.js") }}"></script>
<script src="{{ asset("vendor/magnific-popup/js/jquery.magnific-popup.min.js") }}"></script>
<script src="{{ asset("vendor/float-labels/js/float-labels.min.js") }}"></script>

<!-- Main Javascript File -->
<script src="{{ asset("js/scripts-2.js") }}"></script>

</body>

</html>
