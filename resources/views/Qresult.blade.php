<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Polls Result</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="quest1/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="quest1/assets/css/animate.min.css">
    <link rel="stylesheet" href="quest1/assets/css/fontawesome-all.css">
    <link rel="stylesheet" href="quest1/assets/css/style.css">

<body>
<div class="wrapper">
    <div class="poll-top-head text-center">
        <h1>Résultat de vos ventes actuels</h1>
    </div>
    <div class="poll-top-title text-center">
        <h2>Votre taux de performance actuel</h2>
    </div>
    <!-- /poll top content -->
    <div class="poll-content-img clearfix">
        <div class="poll-result-circle-percent float-left position-relative">
            <div class="pr-value-text">Taux de performance</div>
            <div class="first progress_area position-relative"><strong id="st">{{ $tp }} </strong></div>
        </div>
        <div class="poll-result-text">
            <h3>Votre performance resultat </h3>
            <a href="{{ route('resultat2',['id'=>$lead]) }}">Change Answer</a>
        </div>
    </div>

</div>
<!-- /wrapper -->
<script src="quest1/assets/js/jquery-3.3.1.min.js"></script>
<script src="quest1/assets/js/bootstrap.min.js"></script>
<script src="quest1/assets/js/popper.min.js"></script>
<script src="quest1/assets/js/appear.js"></script>
<script src="quest1/assets/js/circle-progress.js"></script>
<script src="quest1/assets/js/jquery.validate.min.js"></script>
<script src="quest1/assets/js/main.js"></script>
</body>

</html>
