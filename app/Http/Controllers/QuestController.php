<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuestController extends Controller
{
    public function index(){
        return view('Quest');
    }

    public function resultat(Request $request){
      $res1= $request->quiz;
      $res2= $request->fournisseur;
      $res3= $request->contrat;
      $res4= $request->taux;
      $res5= $request->zone;
      $res6= $request->objectif;
      $res7= $request->persona;
      $res8= $request->partie;
      $res9= $request->service;
      $res10= $request->prestation;
      $res11= $request->lead;
      $res12= $request->freq;
      $res13= $request->solution;
      $res14= $request->quantite;
      $lead = (($res6 *1000/( $res10 )) * ($res4));

      $chiffreA = (($res4 * $res14) /100)*($res10);
        $tpp =($chiffreA * 100)/($res6 * 1000);
       $tp= intval($tpp);
    return view('Qresult',['lead'=>$lead,'tp'=>$tp]);
    }
    public function resultat2($id){

        return view('Peresult',['res' =>$id]);
    }
}
