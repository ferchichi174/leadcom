<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class activities extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function categorie()
    {
        return $this->belongsToMany(categorie::class);
    }
}
