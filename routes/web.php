<?php

use App\Http\Controllers\costController;
use App\Http\Controllers\LeadsController;
use App\Http\Controllers\servicesController;
use \App\Http\Controllers\QuestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');
Route::prefix("leads")->group(function(){
    Route::get("Community-manager",[LeadsController::class,"Community"])->name('cmanager');
});
Route::get("services/{id}",[servicesController::class,"services"])->name('services');
Route::get("cost/{id}/{gid}",[costController::class,"cost"])->name('cost');
Route::get("Questionnaire",[QuestController::class,"index"])->name('Quest');
Route::post("resultat",[QuestController::class,"resultat"])->name('resultat');
Route::get("resultat2/{id}",[QuestController::class,"resultat2"])->name('resultat2');
